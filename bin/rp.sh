#!/bin/bash

prod=false

cd `dirname $0`/../work

week=$((`date '+%V'`-1))
dir=`echo $week`revueDePresse
[ ! -d $dir ] && mkdir $dir && echo Generating $dir
cd $dir
echo $dir

#                                             _ _ 
#                             _ __ ___   __ _(_) |
#                            | '_ ` _ \ / _` | | |
#                            | | | | | | (_| | | |
#                            |_| |_| |_|\__,_|_|_|
echo Generating and sending mail to liste-info@april.org
echo "From: echarp <echarpentier@april.org>
To: manu@echarp.org
Subject: Revue de presse pour la semaine $week
" > mail.txt

w3m http://www.april.org/fr/revue-de-presse-par-courriel-sous-forme-de-liste -dump \
	| sed -n "/Revue de presse pour la semaine X/, /^Visitez / p" \
	| sed "s/^\s*•*//g" \
	| sed "s/^\s*//g" \
	| sed "/./,/^$/!d" \
	| sed "/^« /,/ »$/ s/^/> /g" \
	| sed "s/^> « /> /g" \
	| sed "s/ »$//g" \
	| sed "s/\s\([?!:;]\)/\1/g" \
	| sed ':a;N;$!ba;s/\(\/\)\n/\1/g' \
	| sed ':a;N;$!ba;s/:\n\n/:\n/g' \
	| sed ':a;N;$!ba;s/\n:/\:/g' \
	>> mail.txt

sed -i "s/semaine X/semaine $week/g" mail.txt

[[ $prod == true ]] && scp mail.txt echarp.org: && ssh -t echarp.org mutt -H mail.txt
[[ $prod != true ]] && cat mail.txt | less


#                            _                        _ 
#                         __| |_ __ _   _ _ __   __ _| |
#                        / _` | '__| | | | '_ \ / _` | |
#                       | (_| | |  | |_| | |_) | (_| | |
#                        \__,_|_|   \__,_| .__/ \__,_|_|
#                                        |_|            

w3m http://www.april.org/fr/revue-de-presse-sommaire-actualite -dump -cols 1024 \
	| sed -n "/^La <a href/,/<\/ul>/ p" \
	| sed "s/\s*•//g" \
	| sed "s/^\s*//g" \
	| sed ':a;N;$!ba;s/<li>\n//g' \
	| sed ':a;N;$!ba;s/<ul>\n<\/li>/<ul>/g' \
	| sed ':a;N;$!ba;s/\(href="\)\n/\1/g' \
	| sed ':a;N;$!ba;s/\(\/\)\n/\1/g' \
	| sed ':a;N;$!ba;s/\(">\)\n/\1/g' \
	| sed ':a;N;$!ba;s/\n\(">\)/\1/g' \
	| sed ':a;N;$!ba;s/\n\(<\/a>\)/\1/g' \
	> intro.txt

w3m http://www.april.org/fr/revue-de-presse-actualite -dump -cols 1024 \
	| sed -n "/<strong>/,/^\[/ p" \
	| head -n -1 \
	| sed "s/\s*•//g" \
	| sed "s/^\s*//g" \
	| sed "/./,/^$/!d" \
	| sed "s/\s\([?!:;]\)/\1/g" \
	| sed ':a;N;$!ba;s/<strong>\n/<strong>/g' \
	| sed ':a;N;$!ba;s/\n<\/strong>/<\/strong>/g' \
	| sed ':a;N;$!ba;s/:\n\n/:\n/g' \
	| sed ':a;N;$!ba;s/\n\(Lien vers l.article original\)/\1/g' \
	| sed ':a;N;$!ba;s/\(\/\)\n/\1/g' \
	| sed "s/^« /<cite>/g" \
	| sed "s/ »$/<\/cite>/g" \
	> body.txt

rm drupal.txt
cat intro.txt >> drupal.txt
cat body.txt >> drupal.txt

sed -i "s/semaine X/semaine $week/g" drupal.txt

cat drupal.txt | less
