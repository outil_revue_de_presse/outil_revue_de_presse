#!/bin/bash

cd `dirname $0`/../work

# Use the following in tokens.sh to override
#week=$((`date '+%V'`-1))
week=`date '+%-V'`
weekBefore=$((`date '+%V'`-1))
source tokens.sh
dir=`echo $week`revueDePresse
week=`echo $week|sed "s|^0\(.*\)|\1|g"`
[ ! -d $dir ] && mkdir $dir && echo Generating $dir
cd $dir
echo $dir

cp ../../template/linuxfrBegin.html linuxfr.html
cat intro.txt >> linuxfr.html
cat ../../template/linuxfrMiddle.html >> linuxfr.html
cat body.txt >> linuxfr.html
cat ../../template/linuxfrEnd.html >> linuxfr.html

sed -i "s/h3/strong/g" linuxfr.html
sed -i "s/<strong>Sommaire de la revue de presse de l'April pour la semaine X<\/strong>/# Sommaire/g" linuxfr.html
sed -i "s/^<strong>/**/g" linuxfr.html
sed -i "s/<\/strong>$/**/g" linuxfr.html
sed -i "s/<em>/_/g" linuxfr.html
sed -i "s/<\/em>/_/g" linuxfr.html
sed -i "s/semaine X/semaine $week/g" linuxfr.html
sed -i "s/<a href=\"http:\/\/www.april.org\/revue-de-presse\">revue de presse de l'April<\/a>/revue de presse de l'April/g" linuxfr.html
sed -i "s/revue-de-presse-de-l-april-pour-la-semaine-X-de-l-annee-/revue-de-presse-de-l-april-pour-la-semaine-$weekBefore-de-l-annee-/g" linuxfr.html
# Ne pas alourdir inutilement le code avec les URL citées directement : [http://serveur.domaine.tld/chemin/page.html](http://serveur.domaine.tld/chemin/page.html) peut être simplifié en <http://serveur.domaine.tld/chemin/page.html>
sed -i "s/<a href=\"\(.*\)\">\(.*\)<\/a>/[\2](\1)/g" linuxfr.html
sed -i "s|: <a href=\"\(.*\)\">\(.*\)</a>|: <\1>|g" linuxfr.html
sed -i "s/<.\?cite>//g" linuxfr.html
sed -i "s/<.\?ul>//g" linuxfr.html
sed -i "s/<li>/* /g" linuxfr.html
sed -i "s/<.\?li>//g" linuxfr.html
sed -i "s/• /* /g" linuxfr.html
sed -i "s/<br \/>/\n/g" linuxfr.html
sed -i "s|original: \[.*\](\(.*\))|original: \1|g" linuxfr.html

# Préférer le caractère ellipse « … » à trois points, pour les points de suspension
sed -i "s|[ ]*\.\.\.|…|g" linuxfr.html

sed -i "s|\$rails_token|$depeche_token|g" linuxfr.html

#screen w3m linuxfr.html
