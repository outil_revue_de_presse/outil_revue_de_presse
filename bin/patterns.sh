#!/bin/bash

# Some default values
site_pattern='<meta property="og:site_name" content="(.*)".*>'
title_pattern='<title.*>\s*(.*)\s*</title>'
abstract_pattern='<meta name=".escription" content="(.*)".*>'
author_pattern='<meta name=".uthor" content="(.*)".*>'
date_pattern='le <span class="date">(.*)</span>'

case $1 in
  *nextinpact\.com* )
    site='Next INpact'
    title_pattern='<title.*>\s*(.*) - .*\s*</title>'
    author_pattern='<h3><a href=".*" rel="author".*>(.*)</a></h3>'
    date_pattern='<span itemprop="dateCreated" datetime=".*">Publi&#233;e le (.*) &#224; .*</span>'
    abstract_pattern='<strong>(.*) </strong><br />'
    ;;

  *pcinpact\.com\/news\/* )
    site='PC INpact'
    title_pattern='<meta property="og:title" content="(.*)"/>'
    author_pattern='<h3><a href=".*" rel="author".*>(.*)</a></h3>'
    date_pattern='.*e (.*201.) à ..:..'
    abstract_pattern='<strong>(.*) </strong><br />'
    ;;

  *pcinpact\.com\/breve\/* )
    site='PC INpact'
    title_pattern='(.*) - Br&#232;ve PC INpact'
    author_pattern='(Marc Rees)</a>'
    date_pattern='le (.* 201.) à ..:..'
    abstract_pattern='<strong>(.*) </strong><br />'
    ;;

  *pcinpact\.com* )
    site='PC INpact'
    title_pattern='(.*)</h1>'
    author_pattern='<a href=".*">(.*)</a> le  .*  (.* lectures)'
    date_pattern='<a href="mailto.*">.*</a> le  (.*)  (.* lectures)'
    abstract_pattern='<strong>(.*) </strong><br />'
    ;;

  *zdnet\.fr\/blog* )
    title_pattern='.*<title>(.*)( - Actualit.s - ZDNet\.fr)?</title>'
    recode l1..UTF-8 article.html
    author_pattern='.*<span class="author tLoud">par <a .*>(.*)</a></span>'
    date_pattern='.* le <span class="date">(.*)</span>'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *zdnet\.fr* )
    title_pattern='.*<title>(.*)( - Actualit.s - ZDNet\.fr)?</title>'
    author_pattern='<span class="fn">(.*)</span>'
    date_pattern='<time datetime="(.*)">.*</time>'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *zdnet\.com* )
    title_pattern='.*<title>(.*) . ZDNet</title>'
    author_pattern='<Attribute name="author" value="(.*)"/>'
    date_pattern='^<Attribute name="published" value="(.*)T.*"/>'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *silicon\.fr* )
    site='Silicon.fr'
    author_pattern='<span class="date"><span class="meta-prep meta-prep-author">Le\s*</span> <a href=".*" rel="bookmark"><span class="entry-date">.*</span></a></span> <span class="author"><span class="meta-sep">par</span> <span class="vcard"><a class="url fn n" href=".*" title="View all posts by .*">(.*)</a></span> </span> <span class="comments"><a href=".*">.*</a></span>\s*</div>'
    date_pattern='<span class="date"><span class="meta-prep meta-prep-author">Le\s*</span> <a href=".*" rel="bookmark"><span class="entry-date">(.*)</span></a></span> <span class="author"><span class="meta-sep">par</span> <span class="vcard"><a class="url fn n" href=".*" title="View all posts by .*">.*</a></span> </span> <span class="comments"><a href=".*">.*</a></span>\s*</div>'
    ;;

  *cio-online\.com/entretiens* )
    site="cio-online.com"
    author_pattern='par <a href=".*" class="couleur">(.*)</a><br /><br /><div><img.*'
    date_pattern='<p id="maj">Mise &agrave; jour le (.*) .*</p>'
    ;;

  *cio-online\.com* )
    site="cio-online.com"
    recode html article.html
    title_pattern='<div><h1 style="font-weight:bold;">(.*)</h1></div>'
    author_pattern='<br /><b class="couleur">Edition du .*</b> - par <a href=".*" class=".*couleur">(.*)</a>'
    date_pattern='<br /><b class="couleur">Edition du (.*)</b> - par <a href=".*" class=".*couleur">.*</a>'
    ;;

  *lafeuille\.blog\.lemonde\.fr* )
    site='La feuille'
    title_pattern='<title>(.*) . La Feuille</title>'
    author='Hubert Guillaud'
    date_pattern='<a href=".*" title=".*" rel="bookmark"><span class="entry-date">(.*)</span></a>					</div><!-- .entry-meta -->'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *bigbrowser\.blog\.lemonde\.fr* )
    site='Big Browser'
    title_pattern='<title>(.*) . Big Browser</title>'
    author='la rédaction'
    date_pattern='<a href=".*" title=".*" rel="bookmark"><span class="entry-date">(.*)</span></a>\s*</div><!-- .entry-meta -->'
    ;;

  *bugbrother\.blog\.lemonde\.fr* )
    site='Bug Brother'
    title_pattern='<title>(.*) . BUG BROTHER</title>'
    author_pattern='<h2>.* propos de (.*)</h2>'
    date_pattern='<!-- <span class="meta-prep meta-prep-author"></span> --> <a href=".*" title=".*" rel="bookmark">(.*)</a> <!-- <span class="meta-sep">by</span>'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *winch5\.blog\.lemonde\.fr* )
    site='Blog LeMonde.fr'
    title_pattern='<title>(.*) . Winch 5</title>'
    author_pattern='<a href=".*" title=".*" rel="bookmark"><span class="entry-date">.*</span></a><span class="meta-sep">, par </span> <span class="author vcard"><a class="url fn n" href=".*" title="View all posts by .*">(.*)</a></span>					</div><!-- .entry-meta -->'
    date_pattern='<a href=".*" title=".*" rel="bookmark"><span class="entry-date">(.*)</span></a><span class="meta-sep">, par </span> <span class="author vcard"><a class="url fn n" href=".*" title="View all posts by .*">.*</a></span>					</div><!-- .entry-meta -->'
    ;;

  *alternatives\.blog\.lemonde\.fr* )
    site='Blogs LeMonde.fr'
    title_pattern='<h1 class="entry-title">(.*)</h1>'
    author_pattern='<p style="text-align: right;">(.*)...<a href="http://www.twitter.com/.*" target="_blank">.*</a> sur twitter</p>'
    date_pattern='.*<span class="entry-date">(.*)</span></a>					</div><!-- .entry-meta -->'
    ;;

  *blog\.lemonde\.fr* )
    site='LeMonde.fr'
    title_pattern='<h1 class="entry-title">(.*)</h1>'
    author_pattern='<p><strong>(\w* \w*)</strong></p>'
    date_pattern='.*<span class="entry-date">(.*)</span></a>					</div><!-- .entry-meta -->'
    ;;

  *lemonde\.fr\/politique* )
    site='Le Monde.fr'
    recode l1..UTF-8 article.html
    author='la rédaction'
    date_pattern='<p><span itemprop="Publisher">.*</span> . <time datetime=".*" itemprop="datePublished">(.*) &agrave; .*</time> '
    ;;

  *lemonde\.fr\/pixels* )
    site='Le Monde.fr'
    author='la rédaction'
    date_pattern='.*itemprop="datePublished">(.*) &agrave; .*</time>'
    ;;

  *lemonde\.fr* )
    author_pattern='Par      <span itemprop="author" class="auteur txt2_120"><a class="auteur" target="_blank"  href=".*">(.*)</a></span>'
    date_pattern='.*itemprop="datePublished">(.*) &agrave; .*</time>'
    ;;

  *lemondeinformatique\.fr* )
    site='Le Monde Informatique'
    recode l1..UTF-8 article.html
    author_pattern='<div id="signature">Article de <a href=".*">(.*)</a></div>'
    date_pattern='<div class="date cleanprint-dateline">Le (.*)</div>'
    abstract_pattern='<meta name="Description" content="(.*)" />'
    ;;

  *ecrans\.fr* )
    site='écrans'
    title_pattern='.*<title>(.*)- Ecrans.'
    author_pattern='.*<p class="auteur"> par <a href=".*">(.*)</a>'
    date_pattern='.*<p class="date">\s*(.*)\s+.*:'
    abstract_pattern='.*<h3 class="spip"> <i class="spip">(.*)</i> </h3>'
    ;;

  *numerama\.com/magazine* )
    site='Numerama'
    title_pattern='<title>(.*)( - Numerama)?<\/title>'
    author_pattern='Publié par (.*), le '
    date_pattern='<span class="datepublish">(.*)</span>'
    ;;

  *numerama\.com* )
    site='Numerama'
    title_pattern='<title>(.*)( - Numerama)?<\/title>'
    author_pattern='.*<a href="/contact/articles.html" style="text-decoration:underline;color:#929292">(.*)</a> - '
    date_pattern='.*publi&eacute; le (.*) \&.* - '
    ;;

  *pro\.01net\.com* )
    site='01netPro.'
    recode l1..UTF-8 article.html
    author_pattern='.*<div style="border-right: 1px solid #000000; float: left; padding-right: 10px;"><a Onclick="this.href=.*;" class="article_info" style="text-decoration: underline; cursor: pointer;">(.*)</a></div>'
    date_pattern='<div style="border-right: 1px solid #000000; float: left; padding: 0 10px 0 10px;"><span class="article_info">le (.*) à .*</span></div>'
    abstract_pattern='<div class="article_chapeau"><strong>(.*)</strong></div>'
    ;;

  *01net\.com* )
    site='01net.'
    recode l1..UTF-8 article.html
    author_pattern='.*<a .*href=.mailto:commentaires@01net.fr?.* style="text-decoration:.*;">(.*)</a>'
    date_pattern='.*<span class="article_info">le (.*?) à .*</'
    #abstract_pattern='<p class="article_paragraphe"><span class="article_lettrine" style="float: left;">(.*)<br /><br />'
    abstract_pattern='<div class="article_chapeau"><strong(.*)</strong></div>'
    ;;

  *lemagit\.fr* )
    site='LeMagIT'
    title_pattern='<h1>(.*)<\/h1>'
    author_pattern='<a href=".*" class="author">(.*)</a>'
    date_pattern='<meta name="publicationDate" content="(.*)"/>'
    ;;

  *www\.neteco\.com* )
    site='NetEco'
    title_pattern='<title>(.*) par Neteco.com</title>'
    author_pattern='Publi. par <b><a href=\S+ title="(.*?)">'
    date_pattern='<\/a><\/b> le <b>(.*?)<\/b>'
    ;;

  *www\.journaldunet\.com* )
    site='JDN'
    title_pattern='.*<title>(.*) - .*</title>'
    author_pattern='<dt>.*<a href=".*" class="fn.*">(.*)</a></dt>'
    date_pattern='<time .* datetime=".*" pubdate>(.*) ..:.*</time>'
    abstract_pattern='.*<td valign="top" id="chapeau-texte">(.*)</td>'
    ;;

  *www\.generation-nt\.com* )
    title_pattern='.*<title>(.*)<\/title>'
    date_pattern='Le <time .* datetime=".*">(.*) à .*</time> par.*'
    author_pattern='<a .* href=".*" target="_blank">(.*)</a>'
    ;;

  *lepoint\.fr* )
    site='Le Point'
    title_pattern='<title>(.*) - .*</title>'
    author='la rédaction'
    date_pattern='^<meta property="article:published_time" content="(.*)T.*"/>'
    ;;

  *itrmanager\.com* )
    site='ITRmanager.com'
    #title_pattern='<title>(.*) Par .*</title>'
    title_pattern='<title>(.*)</title>'
    #author_pattern='<title>.* Par (.*)</title>'
    author='la rédaction'
    date_pattern='.*<div id="date">(.*)</div>'
    ;;

  *itproportal\.com* )
    site='ITProPortal'
    title_pattern='.*<title>(.*) - ITProPortal.com'
    author='la rédaction'
    date_pattern='.*author_top\">(.*), by '
    ;;

  *readwriteweb\.com* )
    site='ReadWriteWeb'
    title_pattern='.*<title>(.*) [|] '
    author_pattern='.*<a href="http://fr.readwriteweb.com/author/.*".*>(.*)</a>'
    #date_pattern='.*Ecrit le (.* 201.) par '
    date_pattern='.*</div><h3 class="post_date"><div class="jolidate">Ecrit le (.*)</div> <div class=".*">par <a href="http://fr.readwriteweb.com/author/.*/" title=".*">.*</a></div>'
    ;;

  *toolinux* )
    site='toolinux'
    author='la rédaction'
    #date_pattern='.*Posté le (.*) :: '
    date_pattern='.*Publié le (.*)</div>'
    ;;

  *toonux\.com* )
    site='Toonux'
    author_pattern='.*<span class="documentAuthor">Par <a href=".*">(.*)</a></span>'
    date_pattern='^(.*) \d*:\d*$'
    ;;

  *nouvelobs\.com* )
    site="L'OBS"
    title_pattern='.*<h1 itemprop="name">(.*)</h1>.*'
    author_pattern='.*<div class="art-auteur" itemprop="author" itemscope itemtype=".*"><a href=".*" itemprop="url" rel="author" title=".*"><strong>Par <span itemprop="name">(.*)</span></strong>.*'
    date_pattern='.*<time><span>Publié le <a href=".*" title="Actualités du .*">(.*)</a> à .*</span><meta itemprop="datePublished" content=".*"></time>.*'
    abstract_pattern='.*<meta property="og:description" content="(.*)"><meta property="og:url".*'
    ;;

  *webtimemedias\.com* )
    site='WebTimesMedia'
    recode l1..UTF-8 article.html
    title_pattern='.*<title.*>.* - (.*)</title>'
    author_pattern='.*<h2 style="font-size:12px;"><a class="detail_subtitle" href="javascript:.*;">(.*)</a>, le .*</h2>'
    #date_pattern='.*<h2 style="font-size:12px;"><a class="detail_subtitle" href="javascript:.*;">.*</a>, le (.*)</h2>'
    date_pattern='.*<h2 style="font-size:12px;">le (.*)</h2>'
    ;;

  *ladepeche\.fr* )
    site='LaDepeche.fr'
    title_pattern='\s*(.*) - .* - .*</title>'
    author_pattern='<p><time class="date" datetime=".*" pubdate="pubdate">Publié le (.*) à .*</time> </p>'
    author='la rédaction'
    date_pattern='<p><time class="date" datetime=".*" pubdate="pubdate".*>Publié le (.*) à .*</time> </p>'
    ;;

  *itchannel\.info* )
    site='ITChannel.info'
    author='la rédaction'
    date_pattern='.*<div id="date">(.*)</div>'
    ;;

  *pcpro\.co\.uk* )
    site='PCPro'
    title_pattern='.*<title.*>(.*) . News .*</title>'
    author_pattern='.*<p class="quiet minMarginBottom">By (.*)</p>\s'
    date_pattern='.*Posted on (.*) at'
    ;;

  *itworld\.com* )
    site='ITworld'
    title_pattern='.*<title>(.*) . ITworld</title>'
    author='la rédaction'
    date_pattern='.*<span class="publish_date">(.*), .*</span>'
    ;;

  *webdevonlinux\.fr* )
    site='WebDevOnLinux'
    title_pattern='.*<title>(.*)\s*. WebDevOnLinux</title>'
    author_pattern='.*Par <a .*author.*>(.*)</a> le '
    date_pattern='.*</a> le (.*), .*<a.*>'
    ;;

  *webdevonlinux\.fr* )
    site='MyCoop'
    title_pattern='.*<title>(.*)\s*. WebDevOnLinux</title>'
    author='la rédaction'
    date_pattern='.*</a> le (.*), .*<a.*>'
    ;;

  *internetnews\.com* )
    site='internetnews.com'
    title_pattern='\s*(.*) - InternetNews:The Blog -'
    author_pattern='.*By (.*) on '
    date_pattern='.*By .* on (.*) .*:\d*'
    ;;

  *laboiteamonocle\.com* )
    site='La boîte à monocle'
    title_pattern='.*<title>(.*) . La Boîte à Monocle</title>'
    author_pattern='.*">(.*) - aka .*</p>'
    date=''
    ;;

  *tekiano\.com* )
    site='tekiano.com'
    title_pattern='.*<title>(.*)</title>'
    #author_pattern='.*<p align="right"><strong>(.*)</strong></p>'
    author_pattern='<p style="text-align: right;"><strong>(.*)</strong></p><div class="cf"></div>'
    #date_pattern='.*">(.*)</span></td>'
    date_pattern='(.* 201.) ..:..	</span>'
    abstract_pattern='.*<p align="justify"><strong>(.*)</strong></p>'
    ;;

  *theinternets\.fr* )
    site='The Internets'
    title_pattern='.*<title>(.*)</title>'
    author_pattern='.*<p>Par (.*) <span class="edit"></span></p>'
    date_pattern='.*<p>(.*) à .*:.*\s*</p>'
    ;;

  *revue-reseau-tic\.net* )
    site='Revue Réseau TIC'
    title_pattern='.*<title>(.*) - Revue réseau TIC</title>'
    author_pattern='.*<div class="auteur">par\s*<span class="vcard author"><a .*>(.*)</a></span></div>'
    date_pattern='.*<div class="date">Mise en ligne le \s*(.*)</div>'
    ;;

  *theregister\.co\.uk* )
    site='The Register'
    title_pattern='.*<title>(.*) . The Register</title>'
    author_pattern='.*<p class="byline">By <a .*>(.*)</a> '
    date_pattern='.*<p class="dateline">Posted in <a .*>.*</a>, <a .*>(.*)</a></p>'
    ;;

  *agoravox\.fr* )
    site='AgoraVox'
    recode l1..UTF-8 article.html
    title_pattern='.*<title>(.*) - AgoraVox .*</title>'
    author_pattern='.*<h4><a href="auteur/.*" title=".*">(.*)</a></h4>'
    date_pattern='(.*) - <a class="reactions" '
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *agoravox\.tv* )
    site='Agoravox TV'
    title_pattern='.*<title>(.*) - Agoravox .*</title>'
    author_pattern='<span class=.auteurs.><span>par </span> <span class="vcard author"><a class="url fn spip_in" href="auteur/.*">(.*)</a></span>'
    date_pattern='<meta name="date" content="(.*)T.*"/>'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *regardscitoyens\.org* )
    site='Regards Citoyens'
    title_pattern='<title>(.*) &laquo;  Regards Citoyens</title>'
    author='la rédaction'
    date_pattern='<small>(.*)</small>.$'
    ;;

  *bestofmicro\.com* )
    site="Tom's guide"
    author_pattern='.*<a href="#" onclick=".*">(.*)</a>'
    date_pattern='.*<span class="date">.*:.* - (.*)</span>'
    ;;

  *linuxfr\.org* )
    site="linuxfr.org"
    title_pattern='<title.*>(.*) - .*</title>'
    author_pattern='<meta content="(.*)" name="author">'
    date_pattern='<figure class="datePourCss">(.*)</figure>'
    ;;

  *lesinrocks\.com* )
    author_pattern='<div class="name" itemprop="author">par <a href=".*" rel="author" title=".*">(.*)</a></div>'
    date_pattern='.*<div class="date">le.(.*201.) .*</div>'
    abstract_pattern=".*<div class=\"description\"><h2>(.*)</h2></div>.*"
    ;;

  *walf\.sn* )
    site="walfadjri"
    recode l1..UTF-8 article.html
    title_pattern='.*<table width=100% border=0><tr><td width=65% valign=top><table width=100% border=0><tr><td width=65% valign=top><b><font size=2 color=blue>(.*)'
    author='Ibrahima DIOP'
    date='Mercredi 11 août 2010'
    ;;

  *lexpansion.lexpress\.fr* )
    recode l1..UTF-8 article.html
    title_pattern='<title>(.*) - .*</title>'
    author_pattern='tc_vars."article_autor".= "(.*)";'
    date_pattern='tc_vars."article_publication".= .(.*) .*;'
    ;;

  *lexpansion\.com* )
    site="L'Expansion.com"
    recode l1..UTF-8 article.html
    author='la rédaction'
    date_pattern='<h1>.*</h1><p class="info"><span class="auteur">.*</span> - <span class="date">publié le (.*) à .*</span></p>'
    abstract_pattern='.*<div id="chapeau-horizontal" class="texte1"><p><strong>(.*)</strong></p>'
    ;;

  *echosdunet\.net* )
    site='Echos du Net'
    recode l1..UTF-8 article.html
    title_pattern='<title>(.*) - Echos du Net</title>'
    author_pattern='.*Publié par (.*) dans la catégorie '
    date_pattern='.*Publié par .* dans la catégorie .* le (.*)<br />'
    abstract_pattern='.*<div class="chapo"><img src=".*" alt=".*" />(.*)</div>'
    ;;

  *blogs\.mediapart\.fr\/blog\/yanch* )
    site='Mediapart'
    author_pattern='<span class="auteur_article">Par <a href=".*" class="subscriber" title="Le blog de .*">(.*)</a></span>'
    date_pattern='^    *<span class="date_article">(.*)</span>'
    ;;

  *blogs\.mediapart\.fr* )
    site='Mediapart'
    author_pattern='<div class="item"><a href=".*" class="subscriber" title=".*">(.*)</a></div>'
    date_pattern='.*<span class="article-date">(.*)</span> .&nbsp;'
    ;;

  *mediapart\.fr* )
    site='Mediapart'
    title_pattern='<h2>(.*)</h2>'
    author_pattern='<span class="author">Par <a href=".*" class="journalist" title="Tous les articles de .*">(.*)</a></span>'
    date_pattern='.*<span class="article-date">(.*)</span>'
    ;;

  *pro\.clubic\.com\/legislation-loi-internet* )
    site='clubic.com'
    author_pattern='<span>Publié par <a href=".*">(.*)</a></span>'
    date_pattern='<span>le (.* 201.)</span>'
    ;;

  *pro\.clubic\.com* )
    author_pattern='<span>Publié par <a href=".*">(.*)</a></span>'
    date_pattern='<span>le (.*)</span>'
    ;;

  *former_clubic\.com* )
    site='clubic.com'
    recode l1..UTF-8 article.html
    title_pattern='<h1>\/\/ (.*)</h1>'
    author_pattern='.*<b>Publié par</b> (.*)'
    date_pattern='le (.*)'
    abstract_pattern='.*<b>(.*)</b><br />'
    ;;

  *clubic\.com* )
    site='clubic.com'
    recode l1..UTF-8 article.html
    author_pattern='par <span><a href="http://www.clubic.com/auteur/.*">(.*)</a></span>'
    date_pattern='.*le <a href="/actualites-informatique/.*" title=".*">(.*)</a>'
    abstract_pattern='.*<b>(.*)</b><br />'
    ;;

  *lhotellerie-restauration\.fr* )
    site="L'Hôtellerie Restauration"
    recode l1..UTF-8 article.html
    author_pattern='.*<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td width="100%" align="right"><span class="Article_Texte"><b>Par (.*), .*</b></span></td></tr></table><br />'
    date_pattern='.*<span class="Article_Reference">(.*) ..:..</span><br /><font size="1" face="Verdana"><br /></font>'
    ;;

  *pcworld\.fr* )
    title_pattern='.*<title>(.*) . PCWorld.fr</title>'
    author_pattern='.*ar <a href="/infos/auteur.*" rel="author">(.*)</a>.*'
    date_pattern='.*Publié le <time datetime=".*" pubdate>(.*)</time>.*'
    abstract_pattern='.*<p><strong>(.*)</strong></p>'
    ;;

  *zinfos974\.com* )
    site='Zinfos974.com'
    author_pattern='.*<meta name="author" lang="fr" content="(.*)" />'
    date_pattern='.*<div class="access">(.*) - .*:.*</div>'
    ;;

  *lindependant\.com* )
    site="L'Indepéndant.com"
    title_pattern='.*<h1><span class="accroche">.*</span><balise_article_titre>(.*)</balise_article_titre></h1>'
    author_pattern='.*<div align="right" class="signature">(.*)</div>'
    date_pattern='.*<span class="ptxtGris">Edition du (.*)</span>'
    abstract_pattern='.*<strong>(.*)</strong><br>'
    ;;

  *blogs\.lesechos\.fr\/internetactu-net* )
    site='Les Echos'
    author_pattern='^<span class="auteur">(.*)</span> . '
    date_pattern='^(.*201.) . ..:.. '
    ;;

  *blogs\.lesechos\.fr* )
    site='Les Echos'
    title_pattern='\s\+*<h1>(.*)'
    author_pattern='<div class="meta">.* <strong>(.*)</strong>'
    date_pattern='<div class="meta">(.*) . .* . <strong>.*</strong>'
    abstract_pattern='.*<h2><p(.*)</p>'
    ;;

  *lecercle\.lesechos\.fr* )
    site='Le Cercle Les Echos'
    title_pattern='.*<title>(.*) . Le Cercle Les Echos</title>'
    author_pattern='.*<span class="auteur"><a href=".*" title=".*">(.*)</a></span> .'
    date_pattern='.*<span class="date">(.*)</span> .'
    abstract_pattern='.*<h2><p(.*)</p>'
    ;;

  *lesechos\.fr* )
    site='Les Echos'
    author_pattern='<meta itemprop="creator" content=" (.*) " />'
    date_pattern='<meta itemprop="datePublished" content="(.*)" />'
    ;;

  *developpez\.com* )
    site='Developpez.com'
    recode l1..UTF-8 article.html
    author_pattern='.*Un article de (.*)'
    title_pattern='.*<title>(.*)</title>'
    date_pattern='<div class="date" style="margin-top: 18px"><span class="day">(.*)</div>'
    abstract_pattern='<b><font size="1">(.*)</font></b><br/><br/>'
    ;;

  *h-online\.com* )
    site='The H Open Source'
    title_pattern='.*<title>(.*) - The H Open Source: News and Features</title>'
    author_pattern='.*<p>.<a href="mailto:.*" class="noline" title="(.*)">.*</a>.</p>'
    date_pattern='.*<div class="date">(.*), .*</div>'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *ledevoir\.com* )
    site='LeDevoir.com'
    title_pattern='.*<title.*>(.*) . Le Devoir</title>'
    author_pattern='<a href="/auteur/.*">(.*)</a>'
    date_pattern='<span class="date">(.* 201.) .*</span>'
    ;;

  *laseyne\.maville\.com* )
    site='maville.com'
    title_pattern='.*<title.*>(.*) . Le Devoir</title>'
    author_pattern='.*<span class="auteur">		<a href="/auteur/.*/">(.*)</a>'
    date_pattern='</span>&nbsp;								<span class="date">(.*)</span>&nbsp;'
    ;;

  *owni\.fr* )
    site='Owni'
    title_pattern='.*<title>(.*) &raquo; .*</title>'
    #author_pattern='.*<span class="auteur">par <a href=".*" title="Articles par .*">(.*)</a></span>'
    #date_pattern='^                  <span class="date">Le (.*)</span>$'
    author_pattern='<span class="date">Le .*</span> <a rel="author" href=".*" title=".*">(.*)</a> '
    date_pattern='<span class="date">Le (.*)</span> <a rel="author" href=".*" title=".*">.*</a> '
    abstract_pattern='.*<meta name="description" content="(.*)\s*.\s\sOWNI.fr.*" />'
    ;;

  *7sur7\.be* )
    site='i7sur7'
    title_pattern='7s7 Internet - (.*) \(.*\)'
    author='la rédaction'
    date_pattern='.*<span class="date">le (.*)</span>'
    ;;

  *eco\.rue89\.com* )
    site='Eco89'
    title_pattern='.*<title>(.*) . Eco89</title>'
    author_pattern='.*<p class="infos">Par <span class="username">(.*)</span>.*Rue89'
    date_pattern='.*<p class="infos">Par <span class="username">.*</span>&nbsp;.&nbsp;Rue89&nbsp;.&nbsp;(.*)&nbsp;.&nbsp;..H..'
    abstract_pattern='.*<!--paging_filter--><h2 style="text-align: left;">(.*)<br /></h2>'
    ;;

  *blogs.rue89\.com* )
    site='Rue89 Les blogs'
    title_pattern='<meta property="og:title" content="(.*)" />'
    author_pattern='<a href=".*"><img class="avatar" typeof="foaf:Image" src=".*" alt="" /></a>  <a href=".*">(.*)</a><br />'
    date_pattern='<div class="metas date">Publié le (.*) à .*</div>'
    abstract_pattern='.*<!--paging_filter--><p>(.*)<!--break--></p>'
    ;;

  *rue89\.com* )
    author_pattern='<div class="authors"><a href="http://riverains.rue89.com/.*" class="author">(.*)</a> . <span class="situation">'
    title_pattern='<meta itemprop="name" content="(.*)" />'
    date_pattern='.*<span class="date">(.*) à ..h..</span>'
    abstract_pattern='.*<!--paging_filter--><p>(.*)<!--break--></p>'
    ;;

  *rue89lyon\.fr* )
		title_pattern='<meta name="description" content="(.*)"/>'
		author_pattern='<br><span class="author">par (.*)</span>'
		date_pattern='<li class="date">(.*)</li>'
    ;;

  *progilibre\.com* )
    site='Progilibre.com'
    author_pattern='.*<meta name="author" lang="fr" content="Communiqué de (.*)" />'
    date_pattern='.*<div class="access">(.* 201.)</div>'
    ;;

  *framablog\.org* )
    site='Framablog'
    title_pattern='.*<title>(.*) - Framablog</title>'
    author_pattern='.*<meta name="author" content="(.*)" />'
    date_pattern='.*<meta name="date" scheme=".*" content="(.*)T.*" />'
    ;;

  *commentcamarche\.net* )
    site='Commentçamarche.net'
    recode l1..UTF-8 article.html
    author='la rédaction'
    date_pattern='\s*(.* 201.) &agrave; .*:.*</span>'
    abstract_pattern='.*<meta name="description" content="(.*)" /><link rel="canonical"'
    ;;

  *acteurspublics\.com* )
    site='acteurspublics.com'
    title_pattern='<meta property="og:title" content="(.*)" />'
    author_pattern='<span class="meta">.*, PAR (.*)</span>'
    date_pattern='<span class="meta">(.*), PAR .*</span>'
    ;;

  *solutions-logiciels\.com* )
    site='Solutions-Logiciels.com'
    recode l1..UTF-8 article.html
    author_pattern='.*<div class="details_actu"><b>.*</b> - .* le <b>.*</b> - <b>(.*)</b></div>'
    date_pattern='.*<div class="details_actu"><b>.*</b> - .* le <b>(.*)</b> - <b>.*</b></div>'
    ;;

  *electronlibre\.info* )
    site='Electron Libre'
    title_pattern='.*<title.*>(.*) . ElectronLibre.*</title>'
    author_pattern='Publié le <strong>.*</strong> par (.*)</div><!-- .entry-meta -->'
    date_pattern='Publié le <strong>(.*)  </strong> par (.*)</div><!-- .entry-meta -->'
    abstract_pattern='.*<p><strong>(.*)</strong></p><br />'
    ;;

  *slate\.fr* )
    title_pattern='.*<title>(.*) . Slate.*</title>'
    author_pattern='<p><a href=".*">(.*)</a></p>.*<p class="header_infos_rubrique"><a href=".*">.*</a></p>.*<time datetime=".*">.* - .*</time>'
    date_pattern='<p><a href=".*">.*</a></p>.*<p class="header_infos_rubrique"><a href=".*">.*</a></p>.*<time datetime=".*">(.*) - .*</time>'
    ;;

  *starafrica\.com* )
    site='StarAfrica.com'
    title_pattern='.*<title>(.*) : .*</title>'
    author_pattern='.*Auteur : (.*)<br />'
    date_pattern='.*Auteur : .*<br /> (.*) ..:.. '
    abstract_pattern='.*<h2>(.*)</h2>'
    ;;

  *reseaux-telecoms\.net* )
    site='Reseaux-Telecoms.net'
    recode l1..UTF-8 article.html
    title_pattern='<h1>(.*)</h1></div>'
    author_pattern='.*<br /><b class="couleur">Edition du .*</b> - par <a href=".*" class="couleur.*">(.*)</a>'
    date_pattern='.*<br /><b class="couleur">Edition du (.*)</b> - par '
    abstract_pattern='.*<p><strong class="large">(.*)<span> </span><br /></strong></p><p id="txtcontent">'
    ;;

  *technaute\.cyberpresse\.ca* )
    site='technautes.cyberpresse.ca'
    #title_pattern='.*<h1>(.*)</h1> '
    title_pattern='<title>(.*) . Alain McKenna, collaboration spéciale .*</title>'
    author_pattern='.*<div class="auteur"><a href=.*>(.*)</a>'
    date_pattern='\s*(.* 201.)		<span class="sep">.</span>'
    abstract_pattern='.*<p class=.amorce.>(.*)</p><div id="motsliens">'
    ;;

  *lapresseaffaires\.cyberpresse\.ca* )
    site='lapresseaffaires'
    title_pattern='<title>(.*) . .* . Technologie</title>'
    author_pattern='.*<p class=.auteur.><a .*>(.*)</a></p>'
    date_pattern='.*publicatioin_date."(.*) '
    ;;

  *cyberpresse\.ca* )
    site='La Presse'
    title_pattern='<title>(.*) . .* . \w* \w*</title>'
    author_pattern='.*<td valign="middle"><p><a href=".*" alt=".*" title=".*"><strong>(.*)</strong></a><br />'
    date_pattern='.*<p><script language="Javascript">document.write.publicatioin_date."(.*)\s.*", ".*"..;</script>'
    abstract_pattern='<meta name="description" content="(.*)" />'
    ;;

  *itnews\.com\.au* )
    site='itnews'
    title_pattern='.*<h1 class="article-header"><span id=".*">(.*)</span></h1>'
    author_pattern='.*By <a id=".*" href=".*">(.*)</a> on '
    date_pattern='.*<span id="ctl00_ctl00_ContentPlaceHolder1_LeftColumnPlaceHolder_Article_DateTimeLiteral">(.*) \d:.*</span>'
    ;;

  *nordlittoral\.fr* )
    site='NordLittoral'
    recode l1..UTF-8 article.html
    title_pattern='.*<title>(.*) - Nord-Littoral.fr</title>'
    author_pattern='.*<BR/> (.*) <BR/></P><div class="pub"><script language="javascript" type="text/javascript">OAS_AD(.*);</script></div>'
    date_pattern='.*<span class="date">(.*), .*</span>'
    abstract_pattern='<meta name="description" content="(.*)" />.'
    ;;

  *you\.leparisien\.fr* )
    site='You'
    title_pattern='.*<title>(.*) - YOU( – .*)?</title>'
    author_pattern='<div class="PackageDisplay_userName"><a title=".*" href=".*">(.*)</a></div>'
    date_pattern='.*<a class="PackageDisplay_date" title="(.*)" href=".*">'
    abstract_pattern='<meta name="description" content="(.*)"></meta>'
    ;;

  *leparisien\.fr* )
    site='leParisien.fr'
    title_pattern='.*<title>(.*)( – .*)?</title>'
    #author_pattern='<div class="PackageDisplay_userName"><a title=".*" href=".*">(.*)</a></div>'
    author_pattern='(.*)   .             		Publié le .*'
    date_pattern='.*Publié le (.*), ..h..'
    abstract_pattern='<meta name="description" content="(.*)"></meta>'
    ;;

  *thinq\.co\.uk* )
    site='Thinq.co.uk'
    title_pattern='.*<title>(.*) . THINQ.co.uk'
    author_pattern='.*<img src="http://media-thinq.thinq.co.uk/profiles/.*" alt=".*" title="(.*)"/>'
    date_pattern='.*<p class="date">(.*)</p>'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *itespresso\.fr* )
    site='ITespresso'
    author_pattern='^<span class="date">.*<span class="vcard.*"><a class="url fn n" href=".*author/.*" title="View all posts by .*">(.*)</a></span> </span>.*</span>'
    date_pattern='.*<span class="entry-date.*">(.*)</span></a></span>'
    abstract_pattern='<meta name="description" content="([:alpha:]*)" />'
    ;;

  *gamepro\.fr* )
    site='GamePro.FR'
    author_pattern='.*, par <a href="/profil/.*">(.*)</a>'
    date_pattern='.*Publié le (.*),'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *arstechnica\.com* )
    site='ars technica'
    author_pattern='.*<div class="byline"><span class="author">By <a href=".*">(.*)</a>'
    date_pattern='.*<abbr class="timeago datetime" title=".*">(.* 201.) .*</abbr></span></div>'
    ;;

  *europeecologie\.eu* )
    site='europpe écologie'
    title_pattern='.*<title>(.*) - \[Les eurodéputés Europe Écologie\]'
    author_pattern='.*<a href="spip.php\?auteur=.*" title="">(.*)</a>'
    date_pattern='(.* 201.)$'
    ;;

  *syntec-informatique\.fr* )
    site='Syntec informatique'
    author='la rédaction'
    date_pattern='.*<p class="date">.* le (.*)</p>'
    ;;

  *20min\.ch* )
    site='20 minutes online'
    recode l1..UTF-8 article.html
    author='la rédaction'
    date_pattern='<p>(.* 201.) ..:..; '
    ;;

  *lesmobiles\.com* )
    site='lesmobiles.com'
    recode l1..UTF-8 article.html
    author='la rédaction'
    date_pattern='.*<span class="actualite_date">Publi&eacute; le (.*)</span><br />'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *actupparis\.org* )
    site='ActUp Paris'
    title_pattern='.*<title>Act Up-Paris . (.*)</title>'
    author='la rédaction'
    date_pattern='.*<h4>publié en ligne : (.*) </h4>'
    abstract_pattern='.*<meta name="description" content="(.*)">'
    ;;

  *marsactu\.fr* )
    site='marsactu'
    author_pattern='.*<meta name="author" content="(.*)" /><meta name="copyright"'
    date_pattern='.*<meta name="revised" content="(.*), .*" /><meta name="title"'
    abstract_pattern='.*<meta name="description" content="(.*)" /><link rel="image_src"'
    ;;

  *publi-news\.fr* )
    site='Publi News'
    #title_pattern='(.*) </TITRE>'
    author='la rédaction'
    date_pattern='(.*)</DATE>'
    abstract_pattern='.*<meta name="description" content="(.*)" /><link rel="image_src"'
    ;;

  *tekit\.fr* )
    site='tekit.fr'
    author='la rédaction'
    date_pattern='.*<p class="info">.*le (.* 201.)'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *linformaticien\.com* )
    site="L'Informaticien"
    title_pattern='<h1 class=".* articleHeadline">(.*)</h1>'
    author_pattern='<p class="articleAuthor Normal">par (.*), le (.* 201.) .*</p>'
    date_pattern='<p class="articleAuthor Normal">par .*, le (.* 201.) .*</p>'
    abstract_pattern='<p><strong>(.*)</strong></p>'
    ;;

  *atelier\.* )
    site="L'Atelier"
    title_pattern='<title>(.*) . L.Atelier.*</title>'
    author_pattern='Par <a href="/authors/.*" rel="author">(.*)</a>      <span class="date">.*</span>'
    date_pattern='Par <a href="/authors/.*" rel="author">.*</a>      <span class="date">(.*)</span>'
    ;;

  *datanews\.rnews\.be* )
    site='Datanews.be'
    title_pattern='.*<title>(.*) - .*<\/title>'
    author_pattern='.*<em><strong>(.*) <br />'
    #author_pattern='<p class="date">(.*) -'
    date_pattern='\s*(\w* \w* \w* 201.)'
    ;;

  *datanews\.levif\.be* )
    site='Datanews.be'
    title_pattern='.*<title>(.*) - .*<\/title>'
    author_pattern='var redacteur="(.*)";'
    date_pattern='\s*(\w* \w* \w* 201.)$'
    ;;

  *innovationlejournal\.com* )
    site='innovation le journal'
    title_pattern='.*<title>Innovation le journal : (.*)<\/title>'
    author_pattern='.*<div class="article_date">.*, (.*)</div>'
    date_pattern='.*<div class="article_date">(.*), .*</div>'
    abstract_pattern='<meta name="description" content="(.*)" />'
    ;;

  *macgeneration\.com* )
    site='MacGeneration'
    author_pattern='<div class="dateNews">par <a href=".*">(.*)</a> le .* &agrave; .*</div>'
    date_pattern='<div class="dateNews">par <a href=".*">.*</a> le (.*) &agrave; .*</div>'
    ;;

  *macg\.co* )
    site='MacGeneration'
    author_pattern='.*<span class="infos">par <span rel="sioc:has_creator"><span class="username" xml:lang="" about=".*" typeof=".*sioc:UserAccount" property=".*:name" datatype="">(.*)</span></span>.*</span>'
    date_pattern='.*<span class="infos">par <span rel="sioc:has_creator"><span class="username" xml:lang="" about=".*" typeof=".*sioc:UserAccount" property=".*:name" datatype="">.*</span></span> le (.*) à .*</span>'
    ;;

  *sudouest\.fr* )
    site='Sud Ouest'
    title_pattern='.*<title.*>(.*) - SudOuest.fr</title>'
    author_pattern='^Publié le .*<br />Par <span class="sign">(.*)</span>	</p>'
    date_pattern='^Publié le (.*)<br />Par <span class="sign">.*</span>	</p>'
    abstract_pattern='<meta name="description" content="(.*)" />'
    ;;

  *macworld\.fr* )
    site='Macworld.fr'
    author_pattern='.*Publié le .*, par <a href=".*">(.*)</a>'
    date_pattern='.*Publié le (.*)\s*, par <a href=".*">.*</a>'
    ;;

  *programmez\.com* )
    site='Programmez!'
    recode l1..UTF-8 article.html
    title_pattern='.*<title>(.*) . Programmez!</title>'
    author_pattern='<span rel="sioc:has_creator"><span class="username" xml:lang="" about=".*" typeof="sioc:UserAccount" property="foaf:name" datatype="">(.*)</span></span>        </div>	'
    date_pattern='<p class="text_date"><span property="dc:date dc:created" content=".*" datatype="xsd:dateTime">(.*) - .*</span></p>       '
    abstract_pattern='.*<meta name="description" content="(.*)">'
    ;;

  *stateline\.org* )
    site='Stateline'
    author_pattern='.*<div class="story-byline">By (.*), Special to Stateline</div>'
    date_pattern='.*<div class="postedDate">(.*)</div>'
    abstract_pattern='.*<meta name="description" content="(.*)" />'
    ;;

  *branchez-vous\.com* )
    site='Branchez-Vous!'
    title_pattern='.*<title.*>(.*) . Branchez-vous</title>'
    author_pattern='<p>par <span class="auteur_article"><!--print_auteur_debut-->(.*)<!--print_auteur_fin--></span></p>'
    date_pattern='\s*(.* 201.) &agrave; '
    abstract_pattern='<meta name="Description" content="(.*)" />'
    ;;

  *internetactu\.net* )
    site='internet ACTU.net'
    title_pattern='.*<title>(.*) &laquo;  InternetActu.net</title>'
    author_pattern='.*<div class="infoPost">Par <a href="http://www.internetactu.net/author/.*/" title="Posts by .*">(.*)</a>'
    date_pattern='le (.*) . <a href="#commentaires" title="Commentaires">.*</a> . '
    ;;

  *osor\.eu* )
    site='osor.eu'
    title_pattern='<title>(.*) &mdash; </title>'
    author_pattern='<a href="http://www.osor.eu/author/.*">(.*)</a>$'
    date_pattern='(.* 201.)$'
    abstract_pattern='<meta content="(.*)"'
    ;;

  *gizmodo\.fr* )
    site='Gizmodo'
    title_pattern='.*<title>(.*) . Gizmodo.*</title>'
    author_pattern='<div class="entry-info"><span>Par </span><a href=".*" rel="author">(.*)</a>, <span class="entry-date">le <a href=".*">(.*)</a> à <a href="'
    date_pattern='<div class="entry-info"><span>Par </span><a href=".*" rel="author">.*</a>, <span class="entry-date">le <a href=".*">(.*)</a> à <a href="'
    #abstract_pattern='<p><a href="http://cache.gizmodo.fr/.*"><img src=".*" alt="" title=".*" width=".*" height=".*" class="alignleft size-medium .*" /></a>(.*)<span id="more-.*"></span><br/><br/><br />'
    ;;

  *gnet\.tn* )
    site='gnet'
    title_pattern='\s*(.*)</h1></td>'
    author_pattern='<div style="text-align: right;"><strong>(.*)</strong></div><div class="ultimatesbplugin_bottom"><hr/><a rel="nofollow" .* href="https://www.facebook.com/" title="Partagez avec Facebook!" target="_blank"><img height="18px" width="18px" src="http://www.gnet.tn/plugins/content/usbp_images/glossy/facebook.png" alt="Partagez avec Facebook!" title="Partagez avec Facebook!" /></a> <a rel="nofollow" href="http://www.twitter.com/" title="Twitter!" target="_blank"   '
    date_pattern='.*Publié le (.*) à .*	</td>'
    ;;

  *20minutes\.fr* )
    site='20minutes.fr'
    title_pattern='.*<title>(.*) - 20minutes.fr</title>'
    date_pattern='<div class="mna-details">Cr&eacute;&eacute; le (.*) à ..h.. -- Mis &agrave; jour le .*</div>'
    author='la rédaction'
    ;;

  *w3blog\.fr* )
    site='w3blog'
    title_pattern='<title>(.*) . w3blog</title>'
    author_pattern='.* par (.*)            			</span>'
    date_pattern='\s*(.* 201.) par .*            			</span>'
    ;;

  *lci\.fr* )
    site='TF1 news'
    title_pattern='.*<TD class="efnf_titretable" colspan="2">.*00 (.*)</TD>'
    author='la rédaction'
    date_pattern='.*<TD class="efnf_titretable" colspan="2">(.*)\s\d.* .*</TD>'
    ;;

  *lepopulaire\.fr* )
    site='lepopulaire.fr'
    title_pattern='<title>www.lepopulaire.fr - .* - .* - (.*)</title>'
    author_pattern='<p><span class="auteur"><b>(.*)</b></span><br><br></p></texte>'
    date_pattern='(.*1.) - .*'
    abstract_pattern='.*<meta name="description" content="(.*) " />'
    ;;

  *itmag-dz\.com* )
    site='meganews'
    author_pattern='<div class="single-post-author">Written by: <a href=".*" title="Articles par .*" rel="author">(.*)</a></div>'
    date_pattern='<div class="single-post-date">(.*) .*  </div>'
    ;;

  *laligue-alpesdusud\.org* )
    site="la ligue de l'enseignement"
    title_pattern='.*<title>(.*) - Associatifs > LeBlog</title>'
    author_pattern='.*<p class="post-info">Par (.*),'
    date_pattern='(.*) &agrave; .*	<span>::</span> <a href="/associatifs_leblog/.*">.*</a>'
    abstract_pattern='.*<div class="post-chapo"><p><img src=".*" alt="" />(.*)<br />'
    ;;

  *dicosmo\.org* )
    site='My Opinions'
    title_pattern='.*<title>(.*) - My Opinions</title>'
    author_pattern='.*<p class="post-info">Par (.*),'
    date_pattern='\s*(.* 201.) &agrave; .*	<span>::</span> <a href="/MyOpinions/index.php/Informatique-et-societe">Informatique et Société</a>'
    abstract_pattern='.*<div class="post-chapo"><p><img src=".*" alt="" />(.*)<br />'
    ;;

  *artesi-idf\.com* )
    site='artesi'
    recode l1..UTF-8 article.html
    title_pattern='<title>(.*). Artesi .le de France</title>'
    author='la rédaction'
    date_pattern='.*<span class="date">Article du (.*)</span><!-- AddThis Button BEGIN -->'
    abstract_pattern='<meta name="description" content="(.*)"><meta http-equiv="description" content=".*">'
    ;;

  *xboxygen\.com* )
    site='Xboxygen'
    title_pattern='.*<meta name="Description" content="(.*) - .*">'
    author='la rédaction'
    date_pattern='.*<p class="date_article">(.*) - .*</p>'
    abstract_pattern='.*<meta name="Description" content=".* - (.*)">'
    ;;

  *fabula\.org* )
    site='fabula'
    title_pattern='.*<title.*>(.*) \(.*\)</title>'
    author_pattern='<meta name="author" content="(.*)"/>'
    date_pattern='.*Information publiée le (.*) par '
    ;;

  *vousnousils\.fr* )
    site='vousnousils'
    title_pattern='.*<title>(.*)</title>'
    #author_pattern='<br class="clear" /><p><em><strong>(.*)</strong></em></p>		</div>'
    author_pattern='<p><em><strong>(.*)</strong></em></p>		</div>'
    date_pattern='.*<div class="meta"><em class="date">(.*)</em></div>'
    ;;

  *ruefrontenac\.com* )
    site='Rue Frontenac'
    title_pattern='.*<title>Rue Frontenac  -  (.*)</title>'
    author_pattern='.*Écrit par <a href=".*"><span class="small">(.*)</span></a>		</span>'
    date_pattern='\s*([[:alpha:]]*, .* 201.) ..:..	</td>'
    ;;

  *compagnon-parfait\.fr* )
    site='Compagnon-Parfait.fr'
    recode l1..UTF-8 article.html
    author_pattern='.*Publié le .*, par (.*)</font></td><td align=.right.>'
    date_pattern='.*Publié le (.*), par .*</font></td><td align=.right.>'
    abstract_pattern='<meta name="Description" content="(.*)" />'
    ;;

  *usinenouvelle\.com* )
    site="L'usine Nouvelle"
    date_pattern='Publié le <time datetime=".*">(.*), &agrave; .*</time>'
    author_pattern='Par&nbsp;<span itemprop="author">(.*)</span>'
    ;;

  *les-infostrateges\.com* )
    site='les-infostrateges.com'
    recode l1..UTF-8 article.html
    author_pattern='.*<p id="author">Par <b>(.*)</b></p>'
    date_pattern='.*<p id="tags"><span>Publié.? le <strong>(.*)</strong></span><strong>Tags :</strong> <a href=".*" class=".*">.*</a>.</p>'
    ;;

  *ictjournal\.ch* )
    site='ICTjournal'
    title_pattern='.*<h1>(.*)</h1>'
    author_pattern='.*\((.*)\)'
    date_pattern='.*<span class="dateAndAuthor"><i>(.*) .*</i> '
    ;;

  *indexel\.net* )
    site='indexel.net'
    author_pattern='.*Par <a href="mailto:redaction@indexel.net">(.*)</a>      le '
    date_pattern='.*<b>(.* 201.)</b>'
    ;;

  *lalsace\.fr* )
    site="L'Alsace.fr"
    title_pattern='.*<title.*>Alsace . (.*) - L.Alsace</title>'
    author='la rédaction'
    date_pattern='.*<p class="publication">le <span class="date">(.*)</span> &#224; <span class="heure">.*</span></p>'
    ;;

  *telerama\.fr* )
    site='Télérama.fr'
    recode l1..UTF-8 article.html
    title_pattern='<title>(.*) - .* - .*</title>'
    author_pattern='<br><span class="t12-art-meta-aut">(.*)</span>'
    date_pattern='Le (.*) à '
    ;;

  *ouest-france\.fr* )
    site='ouest-france.fr'
    author='la rédaction'
    date_pattern='(.* 201.)$'
    ;;

  *cnetfrance\.fr* )
    site='cnet France'
    recode l1..UTF-8 article.html
    author='la rédaction'
    date_pattern='.*<meta name="date" content="(.*)" />'
    abstract_pattern='<meta name="description" content="(.*)'
    ;;

  *publicsenat\.fr* )
    site='Public Sénat'
    author_pattern='^          (\w* \w*)        </div>$'
    date_pattern='^          Le (.*201.) à .*$'
    ;;

  *altermonde-sans-frontiere\.com* )
    site='Altermonde-sans-frontières'
    title_pattern='<title>\[Altermonde-sans-frontières\] (.*)</title>'
    author_pattern='\s*<a href="spip.php\?auteur.*">(.*)</a>'
    date_pattern='(.* 201.)<br>$'
    ;;

  *webdo\.tn* )
    site='webdo'
    title_pattern='<title>(.*) . Webdo</title>'
    author_pattern='.*<p class="postmeta">Publié par (.*) le .*</p>'
    date_pattern='.*<p class="postmeta">Publié par .* le (.*) (dans\|à) .*</p>'
    ;;

  *journaldugratuit\.com* )
    site='Journal du Gratuit'
    recode l1..UTF-8 article.html
    title_pattern='<td colspan="2" ><IMG SRC=".*"> <A class="txt_18r" HREF=".*">(.*)</a><BR></td>'
    author_pattern='.*<td colspan="2"><a class="txt_9n">.*, par </a><a class="txt_9n" HREF=".*" >(.*)</a><a class="txt_9n"> lu .* fois. \(<A class="txt_9n" HREF="#reaction"> .* réactions\)</A><BR><BR></a></td>'
    date_pattern='.*<td colspan="2"><a class="txt_9n">(.*), par </a><a class="txt_9n" HREF=".*" >.*</a><a class="txt_9n"> lu .* fois. \(<A class="txt_9n" HREF="#reaction"> .* réactions\)</A><BR><BR></a></td>'
    ;;

  *directioninformatique\.com* )
    site='Direction Informatique'
    title_pattern='<title>(.*) . Direction .*</title>'
    author_pattern="_gaq.push..'_setCustomVar', 2, 'Author', '(.*)'..;"
    date_pattern="_gaq.push..'_setCustomVar', 3, 'Pub Date', '(.*)'..;"
    abstract_pattern='.*<meta name="Description" content="(.*)".*>'
    ;;

  *lyon-communiques\.com* )
    site='Lyon-Communiques.com'
    recode l1..UTF-8 article.html
    author='la rédaction'
    date_pattern='.*<h1>.*</h1><h1>.*</h1><i>.*</i><br /><br /><span class=.date.;>.* le (.*)</span><br /><div style="float:left; margin:28px 14px 10px 5px;"><script type="text/javascript">'
    ;;

  *lavantage.qc.ca* )
    site="L'Avantage.qc.ca"
    title_pattern='<title>(.*) - Journal L.Avantage</title>'
    author_pattern='.*.nbsp;Publi.eacute; le .* ..:..	- par (.*)</span>'
    date_pattern='.*.nbsp;Publi.eacute; le (.*) ..:..	- par .*</span>'
    ;;

  *gamekult\.com* )
    site='Gemakult'
    author_pattern='.*<li>Par <b>(.*)</b>, .*</li>'
    date_pattern='.*<li>Téléchargement, post&eacute; le (.*) à .*</li>'
    ;;

  *ledauphine\.com* )
    site='ledauphine.com'
    author='la rédaction'
    date_pattern='.*le (.*201.)'
    ;;

  *actualitte\.com* )
    site='ActuaLitté.com'
    author_pattern='<p><a.*href="mailto:.*" id="lien">(.*)</a> &nbsp;&nbsp;&nbsp;'
    date_pattern='<p class="news10">Le (.*) à .* - .* commentaire.*</p>'
    ;;

  *infodsi\.com* )
    site='infoDSI'
    #author_pattern='<title>.* Par (.*)</title>'
    author='la rédaction'
    date_pattern='.*<div id="date">(.*)</div>'
    ;;

  *jmp\.net* )
    site='Never give up !'
    author_pattern='.*<h4>About (.*)</h4>'
    date_pattern='.*<abbr class="published" title=".*">(.*)</abbr>'
    ;;

  *charentelibre\.fr* )
    site='CharenteLibre'
    title_pattern='.*<title.*>(.*) - CharenteLibre</title>'
    author_pattern='.*<div class="signature">.*<span>(.*)</span></a></div>.* 201. . ..h...*<div class="bdvArticle">'
    date_pattern='.*<div class="signature">.*<span>.*</span></a></div>(.* 201.) . ..h...*<div class="bdvArticle">'
    abstract_pattern='.*</h1><p class="soustitre">(.*)<br/></p>'
    ;;

  *itrnews\.com* )
    site='ITRnews.com'
    date_pattern='.*<div class="article_date">Publié le (.*)</div>'
    ;;

  *contrepoints\.org* )
    site='Contrepoints'
    title_pattern='\s*<title>(.*) . Contrepoints\s*</title>'
    author_pattern='.*<div class="plain-text-wrapper"><p><strong>(.*)</strong></p>'
    date_pattern='<div class="plain-art-meta"><small class="inner-text"><span class="dt-only">Publi.*</span> <span class="s-only">Par <a href=".*" title="Voir tous les articles de l.auteur">(.*)</a>,</span> le <time  class="art-date" datetime=".*" title=".*">(.*)</time> dans <a href=".*" title=".*" rel="category tag">.*</a></small></div>'
    ;;

  *ouestaf\.com* )
    site='ouestaf.com'
    author='la rédaction'
    date_pattern='.*<div class="date">(.*)</div>'
    ;;

  *mediaetudiant\.fr* )
    site='MediaEtudiant.fr'
    author_pattern='.*Publié par <a href=".*" class=".*">(.*)</a>,'
    date_pattern='.*dans <a href=".*" class=".*">.*</a>, le (.*)<div style=".*">'
    ;;

  *businessmobile\.fr* )
    site='business-mobile.fr'
    recode l1..UTF-8 article.html
    author_pattern='.*<p><cite title="Auteur">par <a href=".*">(.*)</a></cite>, .*. Publi&eacute; le <span class="date">.*</span></p>'
    date_pattern='.*<span class="date">(.*)</span>'
    ;;

  *ubergizmo\.com* )
    site='übergizmo'
    #author_pattern='.*<span class="byline" >Par (.*)	        			\s*'
    author_pattern='<span class="byline" ><a href=".*" target=""> (.*)</a>	        			'
    #date_pattern='\s*(.*), 11:06   Paris</span>					</div><!-- /byline_container -->'
    date_pattern='(.*201.)</span>'
    ;;

  *poptronics\.fr* )
    site='poptronics'
    title_pattern='<title> poptronics . (.*)'
    author_pattern='.*<span style="font-size: 80%; vertical-align: top;">(.*)&nbsp;'
    date_pattern='.*<span class="date"> &lt; (.*) &gt;'
    ;;

  *lefigaro.fr/flash-eco* )
    site='Le Figaro.fr'
    recode l1..UTF-8 article.html
    title_pattern='.* - (.*)    </title>'
    author_pattern='<span class="auteur"> Par <a href=".*" title=".*" class="journaliste">(.*)</a></span>		'
    date_pattern='. publi&eacute; <time datetime=".*" pubdate>le (.*) &agrave; .*</time>'
    ;;

  *lefigaro.fr/international* )
    site='Le Figaro.fr'
    author_pattern='<li class="fig-auteur">Par <span itemscope itemtype="http://schema.org/Person" itemprop="author"><a itemprop="name" href="#auteur" class="fig-anchor fig-picto-journaliste-haut">(.*)</a></span></li>'
    date_pattern='<meta name="DC.date.issued" content="(.*)T.*">  <!--heure de publi-->'
    ;;

  *lefigaro.fr/hightech* )
    site='Le Figaro.fr'
    author_pattern='<li class="fig-auteur">Par <span itemscope itemtype="http://schema.org/Person" itemprop="author"><a itemprop="name" href="#auteur" class="fig-anchor fig-picto-journaliste-haut">(.*)</a></span></li>'
    date_pattern='.*<li class="fig-date-pub.*">Publié <time   itemprop="datePublished"  datetime=".*">le (.*) à .*</time></li>'
    ;;

  *lefigaro.fr* )
    author_pattern='<li class="fig-auteur">Par <span itemscope itemtype="http://schema.org/Person" itemprop="author"><a itemprop="name" href="#auteur" class="fig-anchor fig-picto-journaliste-haut">(.*)</a></span></li>'
    date_pattern='le (.*201.) .* ..:..</time>'
    ;;

  *paristechreview\.com* )
    site='ParisTech Review'
    title_pattern='.*<title.*>(.*) . ParisTech Review</title>'
    author='la rédaction'
    date_pattern='.*<div class="meta">.*Rédaction / (.*)</div>'
    ;;

  *quebecos\.com* )
    site='QuebecOS'
    author_pattern='.*<span class="itemPoster">Publié par <a href=".*">(.*)</a></span> <span class="itemPostDate">le .*</span> .<span class="itemStats">.*</span>.'
    date_pattern='.*<span class="itemPoster">Publié par <a href=".*">.*</a></span> <span class="itemPostDate">le (.*201.) .*</span> .<span class="itemStats">.*</span>.'
    ;;

  *distributique\.com* )
    site='Distributique.com'
    recode l1..UTF-8 article.html
    title_pattern='<title.*>(.*) - .* - .*</title>'
    #author_pattern='.*<a href=".*" class="bold">(.*)</a>'
    author_pattern='<span><i>Article de <a class="bold popup_contact" href=".*">(.*)</a></i></span>'
    #date_pattern='.*<span class="rouge">(.*)</span>'
    date_pattern='<div id="surtitre">.* - <span>(.*)</span></div>'
    ;;

  *midilibre\.com* )
    site='MidiLibre.com'
    author_pattern='.*<span class="itemPoster">Publié par <a href=".*">(.*)</a></span> <span class="itemPostDate">le .*</span> .<span class="itemStats">.*</span>.'
    date_pattern='.*<span class="itemPoster">Publié par <a href=".*">.*</a></span> <span class="itemPostDate">le (.*201.) .*</span> .<span class="itemStats">.*</span>.'
    ;;

  *siteduzero\.com* )
    site='le Site du Zéro'
    title_pattern='<h1>(.*)</h1>'
    author_pattern='^                <a href="membres-.*">(.*)</a>'
    date_pattern='.*<strong>Publié</strong> : le (.*) à .*<br />'
    ;;

  *latribune\.fr\/technos-medias* )
    site='La Tribune'
    author_pattern='<span class="signature">(.*)</span>'
    date_pattern='(.*201.), ..:.. - 							.* mots												</span>'
    ;;

  *latribune\.fr* )
    site='La Tribune'
    recode l1..UTF-8 article.html
    author_pattern='<span class="signature">(.*)</span>'
    date_pattern='(.*201.), ..:.. - 							.* mots												</span>'
    ;;

  *softonic\.fr* )
    title_pattern='.*<title.*>(.*) . Actualites . Softonic</title>'
    author_pattern='<dd><a title=".*" href=".*" rel="author">(.*)</a> .</dd>'
    date_pattern='<dd> (.*)</dd>'
    ;;

  *francemobiles\.com* )
    site='france mobiles.com'
    recode l1..UTF-8 article.html
    author='la rédaction'
    date_pattern='<div class="theDate">(.*)</div>'
    ;;

  *lepartidegauche\.fr* )
    site='Parti de Gauche'
    author='la rédaction'
    date_pattern='(.* 201.) .*		</span>'
    ;;

  *igeneration\.fr* )
    site='igeneration'
    author_pattern='.* - <a rel=.nofollow. href=.*>(.*)</a>'
    date_pattern='(.*) - .* - <a rel=.*>.*</a>'
    ;;

  *touteleurope\.eu* )
    site='Toute lEurope'
    author='la rédaction'
    ;;

  *inaglobal\.fr* )
    site='ina global'
    recode l1..UTF-8 article.html
    title_pattern='.*<title.*>.* - (.*)</title>'
    author_pattern='.*<a href=".*" class="contributeur">(.*)</a>		</div>'
    date_pattern='.*Publi.* le  <span class="grey">(.*)</span><br/>.* <span class="grey">.*</span>'
    ;;

  *zone-numerique\.com* )
    site='Zone Numerique'
    author_pattern='Publié le .* par \s*(.*)\s*</div>'
    date_pattern='Publié le \s*(.*)\s* - .* par .*</div>'
    ;;

  *degroupnews\.com* )
    site='DegroupNews'
    title_pattern='.*<title.*>(.*) - .*</title>'
    author_pattern='.*<span class="info_lien_news">Br.ve r.dig.e le .* &agrave; .* par (.*)</span>'
    date_pattern='.*<span class="info_lien_news">Br.ve r.dig.e le (.*) &agrave; .* par .*</span>'
    ;;

  *argent\.canoe\.ca* )
    site='canoe'
    recode l1..UTF-8 article.html
    author_pattern='^(\w.*)<br>'
    date_pattern='<div id="nouv_date">(.*) . ..h.. </div>'
    ;;

  *fr\.canoe\.ca* )
    site='canoe'
    recode l1..UTF-8 article.html
    author_pattern='.*<td class="auteur">.* <br>(.*)<br>'
    author='la rédaction'
    date_pattern='<time datetime=".*">(.*) . ..h..</time>'
    ;;

  *canoe\.ca* )
    site='canoe'
    title_pattern='<META NAME="Title" CONTENT="(.*)" />'
    author_pattern='.*<td class="auteur">.* <br>(.*)<br>'
    date_pattern='.*<span class="datetc">(.*) .*&nbsp;'
    abstract_pattern='.*<td class="vignette">(.*).nbsp; <br>'
    ;;

  *sciencesetavenir\.fr* )
    site='Sciences et Avenir.fr'
    title_pattern='<title>(.*) - High-tech - .*</title>'
    author_pattern='.*<strong>Propos recueillis par (.*)</strong><br /><em><strong> Sciences et Avenir.fr</strong></em><br /> .*</p>'
    date_pattern='.*<strong>Propos recueillis par .*</strong><br /><em><strong> Sciences et Avenir.fr</strong></em><br /> (.*)</p>'
    ;;

  *e-alsace\.net* )
    site='e-alsace.net'
    title_pattern='\s*<h3>(.*)</h3>$'
    author='la rédaction'
    date_pattern='.*<span class="dateArticle">(.* 201.) .*</span>'
    ;;

  *lunion\.presse\.fr* )
    site="L'Union"
    title_pattern='(.*) . L.Union</title>'
    author='la rédaction'
    date_pattern='\s*Publié le (.* 201.) '
    ;;

  *localtis\.info* )
    site='Localtis.info'
    recode l1..UTF-8 article.html
    title_pattern='<title>(.*) - .*</title>'
    author_pattern='<p class="signature">(.*)</p>'
    date_pattern='.*Publié le (.* 201.).*</p>'
    ;;

  *lyoncapitale\.fr* )
    site='Lyon Capitale.fr'
    title_pattern='<title>(.*)'
    author='Florent Deligia'
    date_pattern='Publi.*le&nbsp;(.*201.)&nbsp;'
    ;;

  *webactus\.net* )
    site='WebActus'
    title_pattern='.*<title.*>(.*) . WebActus</title>'
    author_pattern='.*<div>A propos de  (.*)</div>'
    date_pattern='<img src=".*" alt=""/>(.* 201.)                            <span class="comments">.*</span>'
    ;;

  *romandie\.com* )
    site='Romandie'
    title_pattern='.*<title.*>CH/TF: (.*)</title>'
    author='la rédaction'
    date=''
    ;;

  *streetgeneration\.fr* )
    site='Street Generation'
    title_pattern='(.*)</title>'
    author_pattern='<meta name=.joliprint.author. content=.(.*). />'
    date_pattern='<meta name=.joliprint.date. content=.(.*). />'
    ;;

  *zebulon\.fr* )
    site='Zebulon.fr'
    recode l1..UTF-8 article.html
    author_pattern='\s*Par (.*) . le .* à .* '
    date_pattern='\s*Par .* le (.*) à .* '
    ;;

  *lexpressmada\.com* )
    site="L'Express de Madagascar"
    recode l1..UTF-8 article.html
    title_pattern='\s*<title>(.*) \(.*\) - .*</title>'
    author_pattern='\s*<div class="article-auteur">(.*)</div>'
    date_pattern='\s*<div class="article-date">(.*)</div>'
    ;;

  *zorgloob\.com* )
    site='Zorgloob'
    recode l1..UTF-8 article.html
    title_pattern='.*<title.*>(.*). &#8211;  Zorgloob.com : .*</title>'
    author_pattern='\s*<p class="byline-author-Luka">Par (.*)</p>'
    date_pattern='<p class="datearticle">Le (.*)</p>						'
    abstract_pattern='\s*</div><!-- .post-thumb --> <strong> <p>(.*)</p>'
    ;;

  *lesnumeriques\.com* )
    site='Les Numeriques'
    #author_pattern='<div class="PDate2">(.*) <br />'
    author_pattern='(\w* \w*) <br />'
    #date_pattern='<div class="PDate2">.*<br />(.* 201.) '
    date_pattern='(.* 201.) ..:..'        
    abstract_pattern='<strong>(.*)</strong><br />'
    ;;

  *biladi\.fr* )
    site='Biladi'
    author_pattern='<p>(.*) "biladi.fr" </p><!-- google_ad_section_end -->'
    date_pattern='.*<span class="date1">(.*)</span>'
    ;;

  *nordeclair\.fr* )
    site='Nord éclair'
    recode l1..UTF-8 article.html
    title_pattern='.*<link rel="apple-touch-icon" href="/iphone_icon.png" /><title>(.*) - Nord Éclair.*</title>'
    author='la rédaction'
    date_pattern='.*<p class="story_date">Publi.* le (.* 201.) .*</p>'
    ;;

  *tunisiehautdebit\.com* )
    site='Tunisie Haut débit'
    author_pattern='<p style="text-align: right;"><strong>(.*)</strong></p>'
    date_pattern='.*<div class="date_day">(.*)</div>'
    ;;

  *le-tigre\.net* )
    site='Le Tigre'
    recode l1..UTF-8 article.html
    author_pattern='.*<div class="bande texte bistre caps detail">Par <a href=".*">(.*)</a></div>'
    date_pattern='<div class="date_publi">Mis en ligne le (.*) ; mis &agrave; jour le .*.</div>'
    ;;

  *epn-ressources\.be* )
    site='EPN de Wallonie'
    author_pattern='.*par <span class="vcard author"><a href=".*" class="url fn">(.*)</a></span> le <abbr class="published" title=".*">.*</abbr>					</span>'
    date_pattern='.*par <span class="vcard author"><a href=".*" class="url fn">.*</a></span> le <abbr class="published" title=".*">(.*)</abbr>					</span>'
    ;;

  *humanite\.fr* )
    site="l'Humanité.fr"
		title_pattern='<meta property="og:title" content="(.*)" />'
    author_pattern='.*<p class="author">(.*)</p>.*'
		date_pattern='.*<span class="date-display-single">(.*)</span>.*'
    abstract_pattern='<p class="chapo">(.*)'
    ;;

  *presence-pc\.com* )
    site="tom's hardware"
    author_pattern='<a href="#" onclick="...form_cont...submit.." itemprop="author">(.*)</a>$'
    date_pattern='<span datetime=".*" itemprop="datePublished">.* - (.*)</span>'
    ;;

  *tomshardware\.fr* )
    author_pattern='Par <a class="author spaceR5 crLink" href="#" onmouseover=".*">(.*)</a><span class="date upper cInfos">.*</span>        </div>      '
    date_pattern='Par <a class="author spaceR5 crLink" href="#" onmouseover=".*">.*</a><span class="date upper cInfos">(.*)</span>        </div>      '
    ;;

  *webmanagercenter\.com* )
    site="wmc actualités"
    title_pattern='<title>(.*) . Directinfo</title>'
    author_pattern='<span style="color:.*;  font-weight:bold;">Par : </span>	<a href=".*">\s*(.*) </a>\s*'
    date_pattern='\s*.. <span style="color:.*;">(.*)</span> . <span style="color:.*;">.*</span> .'
    ;;

  *erenumerique\.fr* )
    site="Ere numerique"
    recode l1..UTF-8 article.html
    author_pattern="<td width='.*' valign='top' align='right'><font face='arial' size='2' color='#ec640c'>par  <span itemprop=.reviewer.>(.*)</span></td>"
    date_pattern="<td width='327' valign='top' align='left'><font face='arial' size='2' color='#ec640c'>. <time itemprop=.dtreviewed. datetime=..*.>(.*)</span> .</td>"
    #author_pattern="<td width='.*' align='right'><font face='arial' size='2' color='.*'>.* - Par <b>(.*)</b></font></td>"
    #date_pattern="<td width='.*' align='right'><font face='arial' size='2' color='.*'>(.*) - Par <b>.*</b></font></td>"
    ;;

  *lenouveleconomiste\.fr* )
    site='Le nouvel Economiste'
    title_pattern='^<title>(.*) . Le nouvel Economiste</title>'
    author_pattern='<p><em>Par (.*)</em></p>'
    date_pattern='<span class="meta-prep meta-prep-author">Publi&eacute; le</span> <a href=".*" title=".*" rel="bookmark"><span class="entry-date">(.*)</span></a>'
    ;;

  *idboox\.com* )
    site='IDBoox'
    title_pattern='.*<title.*>(.*) . IDBOOX</title>'
    author_pattern='<b>.* &middot;</b></div> <!-- fin div  datesingle2-->  <div id="authorarticle"><a href=".*" title="Articles par .*" rel="author">(.*)</a> </div>  <!-- fin div  authorarticle-->'
    date_pattern='<b>(.*) &middot;</b></div> <!-- fin div  datesingle2-->  <div id="authorarticle"><a href=".*" title="Articles par .*" rel="author">.*</a> </div>  <!-- fin div  authorarticle-->'
    ;;

  *quechoisir\.org* )
    site='Que Chosir.org'
    title_pattern='.*<title>(.*)\s*- Que Choisir en ligne'
    author=''
    date_pattern='(.*201.)$'
    ;;

  *siliconmaniacs\.org* )
    site='Silicon Maniacs'
    title_pattern='.*<title>(.*)\s*. Silicon Maniacs</title>'
    author='la rédaction'
    date_pattern='<div class="entry-posted"><span class="meta-prep meta-prep-author">Posted on</span> <span class="entry-date">(.*)</span> <span class="meta-sep">by</span> <span class="author vcard"><a class=".*" href=".*" title=".*">.*</a></span></div>'
    ;;

  *presse-citron\.net* )
    site='Presse Citron'
    author_pattern='.*Par <a href="/author/.*">(.*)</a>'
    date_pattern='.*<span class="dateheure">(.*) :: .* :: Par'
    ;;

  *cdurable\.info* )
    site='CDurable.info'
    title_pattern='<title>CDURABLE.info l&#8217;essentiel du développement durable : : (.*)</title>'
    author='la rédaction'
    date_pattern='.*<h6>(.*).</h6>'
    ;;

  *minutebuzz\.com* )
    site='minute buzz'
    title_pattern='.*<title.*>(.*) - .*</title>'
    author_pattern='.*href="http://www.minutebuzz.com/author/.*" title="Articles par .*">(.*)</a>'
    date_pattern='src="http://www.minutebuzz.com/wp-content/uploads/userphoto/.*" alt=".*" width="25" height="25" class="photo" /> (.*) <a'
    ;;

  *begeek\.fr* )
    site='Be Geek'
    author_pattern='.*<p class="post-info">Publi&eacute; le 6 mai 2011 par <a href="http://www.begeek.fr/author/.*">(.*)</a> . </p>'
    date_pattern='.*<p class="post-info">Publi&eacute; le (.*) par <a href="http://www.begeek.fr/author/.*">.*</a> . </p>'
    ;;

  *bienpublic\.com* )
    site='Le Bien Public'
    title_pattern='.*<title.*>\w* . (.*) - .*</title>'
    #author_pattern='<p><strong>(.*)</strong></p>'
    author='la rédaction'
    #date_pattern='.*<p class="publication">Publi&#233; le <span class="date">(.*)</span></p>'
    date_pattern='le (.*)'
    ;;

  *tunisiait\.com* )
    site='Tunisia IT'
    title_pattern='.*<span  class="Titre-une" style="text-align:justify">(.*)</span>'
    author_pattern='.*<span style="font-weight: bold;">(.*)</span>'
    date_pattern='.*<p class="publication">Publi&#233; le <span class="date">(.*)</span></p>'
    ;;

  *spectrum\.ieee\.org* )
    site='IEEE Spectrum'
    title_pattern='<title.*>(.*) - .*</title>'
    author_pattern='.*<p class="articleBodyTtl"><strong>POSTED BY:</strong>  (.*) &nbsp;/&nbsp; .*</p>'
    date_pattern='.*<p class="articleBodyTtl"><strong>POSTED BY:</strong>  .* &nbsp;/&nbsp; (.*)</p>'
    ;;

  *pipelette\.com* )
    site='Pipelette.Com'
    author='la rédaction'
    date_pattern='<span class="date">(.*)</span>'
    ;;

  *zataz\.com* )
    site='Zataz.com'
    recode l1..UTF-8 article.html
    title_pattern='.*<title.*>(.*)</title>'
    author='la rédaction'
    date_pattern='(.*201.) <b>&agrave;</b> .*</a></span>'
    ;;

  *gamers\.fr* )
    site='gamers.fr'
    author_pattern='.*Web 		du .*		par <strong>(.*)</strong> -'
    date_pattern='.*Web 		du (.*)		par <strong>.*</strong> -'
    ;;

  *theinquirer\.net* )
    site='the Inquirer'
    title_pattern='^<meta property="og:title" content="(.*)" />'
    date_pattern='(.* 201.), '
    author_pattern='By <a href="http://www.theinquirer.net/inquirer/flame_author/.*">(.*)</a>'
    ;;

  *theinquirer\.fr* )
    site='the Inquirer'
    title_pattern='.*<title.*>(.*) - .* - .* - .*</title>'
    author_pattern='.*<p class="postedBy">Par : <a href=".*">(.*)</a> - .*</p>'
    date_pattern='.*<p class="postedBy">Par : <a href=".*">.*</a> - (.*) à .*</p>'
    ;;

  *pockett\.net* )
    site='Pockett'
    recode l1..UTF-8 article.html
    title_pattern='<title.*>(.*) - .*</title>'
    author_pattern='.*<div class=.newsdate.>Posté par <b>(.*)</b> à .*</div>'
    date_pattern='.*<div class=.newsdate.>Posté par <b>.*</b> à .* le (.*)</div>'
    abstract_pattern='.*<div class=.newscontent.><b> .* - (.*)  </b>'
    ;;

  *liberation\.fr* )
    site='Libération.fr'
    title_pattern='.*<title.*>(.*) - .*</title>'
    author_pattern='.*<span itemprop="author creator">(.*)</span></a></span>'
    date_pattern='.*<time class="time" datetime=".*">(.*)</time>'
    ;;

  *charliehebdo\.fr* )
    site='Charlie Hebdo'
    title_pattern='<title>(.*)</title>'
    author_pattern='.*<small>Par <strong>(.*)</strong></small>'
    date_pattern='<h5><a href="http://www.liberation.fr.*">Économie</a>  (.*) à .* </h5>'
    abstract_pattern='.*<meta name="Description" content="(.*)".*>'
    ;;

  *categorynet\.com* )
    site='Categorynet.com'
    title_pattern='<title>(.*) . Categorynet.com</title>'
    date_pattern='(.* 201.) ..:..		</span>'
    ;;

  *atlantico\.fr* )
    site='atlantico'
    title_pattern='.*<title>(.*) . Atlantico</title>'
    author='la rédaction'
    date_pattern='<div class="metas">Publié le (.*)</div>'
    ;;

  *lepost\.fr* )
    site='LePost'
    title_pattern='<title>(.*) - .*</title>'
    author_pattern='<title>.* - (.*) .*</title>'
    date_pattern='.*<p class="articleInfos">(.*201.) &agrave;.* mis '
    ;;

  *lentreprise.lexpress\.fr* )
    site="L'Entreprise.com"
    recode l1..UTF-8 article.html
    title_pattern='<title>(.*) - .*</title>'
    author_pattern='.*<p class="author">(.*) pour LEntreprise.com, publié le <time datetime=".*">.*</time></p>'
    date_pattern='.*<p class="author">.*, publié le <time datetime="(.*)">.*</time></p>'
    ;;

  *lexpress\.fr* )
    site="L'Express.fr"
    recode l1..UTF-8 article.html
    title_pattern='<title>(.*) - .*</title>'
    author_pattern='.*<p class="authors">(.*), publi.* le'
    date_pattern="tc_vars..article_publication..= '(.*) .*';"
    ;;

  *capital\.fr* )
    site='Capital.fr'
    title_pattern='<title>(.*) - .*</title>'
    date_pattern='<span id="ac_dateStyleBis">(.*) &agrave; ..:.. / </span>'
    author='la rédaction'
    ;;

  *politis\.fr* )
    site='Politis.fr'
    title_pattern='<title>(.*) - .*</title>'
    author_pattern='Par <span class="vcard author"><a class="url fn spip_in" href=".*">(.*)</a></span>.*'
    #date_pattern='.*<p><small><abbr class="published" title=".*">(.*)</abbr>, par  <span class="vcard author"><a class="url fn spip_in" href=".*">.*</a></span></small></p>'
    date_pattern='<abbr class="published" title=".*">(.* 201.)</abbr>'
    ;;

  *mediacongo\.net* )
    site='mediacongo.net'
    recode l1..UTF-8 article.html
    title_pattern='.*<NewsTitle>(.*)</NewsTitle>'
    author='la rédaction'
    date_pattern='.*<p class="date">Le 								(.*) '
    ;;

  *nonfiction\.fr* )
    site='nonfiction.fr'
    title_pattern='<title>(.*) - .*</title>'
    author_pattern='<a href="http://www.nonfiction.fr/.*">(.*)</a>'
    date_pattern='<div class="date">\[(.*) - .*\]</div>'
    ;;

  *mondequibouge\.be* )
    site='Mondequibouge.be'
    title_pattern='<title>(.*) .laquo. .*</title>'
    author_pattern='<p><strong>(.*)</strong></p>'
    date_pattern='.*<small>(.*)</small><br/>'
    abstract_pattern='<div id="chapeau">(.*) </div>'
    ;;

  *larevolucionvive\.org\.ve* )
    site='La Révolucion VIVE'
    title_pattern='<title>(.*) - .*</title>'
    author='la rédaction'
    date_pattern='^				(.* 201.)'
    ;;

  *dna\.fr* )
    site='DNA'
    title_pattern='<title>.* - (.*) - .*</title>'
    author='la rédaction'
    date_pattern='.*<p class="publication">le <span class="date">(.*)</span> <span class="heure">.*</span></p>'
    ;;

  *portail-femme\.com* )
    site='Portail Femme'
    author_pattern='.*<p style="margin-top: 15px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 16px; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; border-width: 0px; padding: 0px">(.*) </p>'
    date_pattern='(.* 201.) ..:..				</span>'
    ;;

  *newzilla\.net* )
    site='NewZilla.net'
    title_pattern='.*<h2 class="singlePageTitle">CHRONIQUE > (.*)</h2>'
    author_pattern='.*<p class="singlePostMeta"><img alt=.. src=..*. class=.avatar avatar-28 photo. height=.28. width=.28. />		Par  <a href=".*" title="Articles par .*">(.*)</a> '
    date_pattern='.*le (.*) . Class.'
    ;;

  *taiwaninfo\.nat\.gov\.tw* )
    site='Taiwan Info'
    title_pattern='.*<h3>(.*)</h3>'
    author='la rédaction'
    date_pattern='<p><em>(.*) </em><br><em></em></p>'
    ;;

  *euractiv\.com* )
    site='EurActiv'
    title_pattern='.*<title>(.*) . EurActiv</title>'
    author='la rédaction'
    ;;

  *euractiv\.fr* )
    site='EurActiv'
    title_pattern='.*<title>(.*) . EurActiv.*</title>'
    author_pattern='.*<div class="field-items"><div class="field-item even"><a href="http://www.euractiv.fr/authors/.*">(.*)</a></div></div></div><div id="node_eanews_full_group_translation".*'
    date_pattern='.*<strong>Published:</strong> (.*) - .* . <strong>Updated:</strong> .*</div></div>'
    ;;

  *toutmontpellier\.fr* )
    site='Tout Montpellier'
    author_pattern='<br/><span style="font-size:14px;">Publié par <a>(.*)</a> le .*</span> '
    date_pattern='<br/><span style="font-size:14px;">Publié par <a>.*</a> le (.*) à .*</span> '
    ;;

  *article11\.info* )
    site='Article XI'
    title_pattern='.*<title>(.*) - .*</title>'
    author_pattern='<p align="right"><small>.*, par  <a href=".*">(.*)</a></small></p>'
    date_pattern='<p align="right"><small>(.*), par  <a href=".*">.*</a></small></p>'
    ;;

  *francetv\.fr* )
    site='francetv info'
    title_pattern='<title>(.*) - .*</title>'
    author='la rédaction'
    date_pattern='<div class="timestamp">             Publié le (.*) '
    ;;

  *francetvinfo\.fr* )
    site='francetv info'
    author_pattern='author: "(.*)",'
    date_pattern=', publié le <time datetime=".*" itemprop="datePublished">(.*) . ..:..</time>'
    ;;

  *abondance\.com* )
    site='Abondance Actualité'
    title_pattern='.*<title>(.*) - .*</title>'
    author='la rédaction'
    date_pattern='<span class="post-body"> <i>Le (.*)</i>'
    ;;

  *elmoudjahid\.com* )
    site='El Moudjahid'
    author_pattern='<strong>(.*) </strong></p>                                        </div>'
    date_pattern='<span>PUBLIE LE : (.*201.) . .*</span>'
    ;;

  *veilleur-strategique\.eu* )
    site='Veilleur Stratégique'
    title_pattern='.*<title>(.*) . Veilleur .*</title>'
    author_pattern='<span class="meta-prep meta-prep-author">Publié le</span> <span class="entry-date">.*</span> <span class="meta-sep">par</span> <span class="author vcard">(.*)</span>					</div><!-- .entry-meta -->'
    date_pattern='<span class="meta-prep meta-prep-author">Publié le</span> <span class="entry-date">(.*)</span> <span class="meta-sep">par</span> <span class="author vcard">.*</span>					</div><!-- .entry-meta -->'
    ;;

  *blogeee\.net* )
    site='blogeee.net'
    title_pattern='.*<title>(.*) . blogeee.net</title>'
    author_pattern='<div class="column span-8 metadata">Le <a href=".*">.*</a> &agrave; .* par <a href=".*" title=".*" rel="author">(.*)</a> <span class="comments"><a href=".*" title=".*">.*</a></span></div>'
    date_pattern='<div class="column span-8 metadata">Le <a href=".*">(.*)</a> &agrave; .* par <a href=".*" title=".*" rel="author">.*</a> <span class="comments"><a href=".*" title=".*">.*</a></span></div>'
    ;;

  *reflets\.info* )
    site='reflets.info'
    title_pattern='.*<title>(.*) . Reflets</title>'
    author_pattern='<div class="date">.*</div>                Par <a href=".*" title=".*" rel="author">(.*)</a> '
    date_pattern='<div class="date">(.*)</div>                Par <a href=".*" title=".*" rel="author">.*</a> '
    ;;

  *agro-media\.fr* )
    site='agro-media.fr'
    title_pattern='.*<title>(.*) . agro-media.fr</title>'
    author='la rédaction'
    date_pattern='<span class="submitted">(.*)</span>'
    ;;

  *come4news\.com* )
    site='come4news'
    recode l1..UTF-8 article.html
    author_pattern='.* visites -  Flux   <a href=".*">.*</a> - Ecrit par<a href=".*" >  (.*)  </a> - Lire <a href=".*" title="Abonnez-vous au flux de ce reporter C4N">  son flux RSS </a>'
    date_pattern='(.*201.) .* visites -  Flux   <a href=".*" title="Abonnez-vous au flux de ce theme C4N">.*</a> - Ecrit par<a href=".*" >.*</a> - Lire <a href=".*" title="Abonnez-vous au flux de ce reporter C4N">  son flux RSS </a>'
    ;;

  *franceinter\.fr* )
    site='France Inter'
    title_pattern='.*<title>(.*) . France Inter</title>'
    author_pattern='<div class="date">.*</div>                Par <a href=".*" title=".*" rel="author">(.*)</a> '
    date_pattern='l.émission du <strong class="rouge">(.*)</strong>'
    ;;

  *franceinfo\.fr* )
    site='France Info'
    title_pattern='<title>(.*) . France info</title>'
    author_pattern='<a href="/personne/.*">(.*)</a>      </span>'
    date_pattern='<span class="date">(.*)</span>'
    ;;

  *franceculture\.fr* )
    site='France Culture'
    title_pattern='<title>(.*) - .* - .*</title>'
    author='la rédaction'
    date_pattern='<span class="date">(.*) - .*</span>'
    ;;

  *tunisiesoir\.com* )
    site='Tunisie Soir'
    title_pattern='.*<title>(.*) . Tunisie Soir .*</title>'
    author='la rédaction'
    date_pattern='<li><a href="/general/(....11)-.*" title="Skip to content">Skip to content</a></li>'
    ;;

  *histoire-cigref\.org* )
    site='Histoire CIGREF'
    title_pattern='\s*(.*) &raquo; Histoire CIGREF</title>'
    author='la rédaction'
    date_pattern='<div class="date">(.*)</div>'
    ;;

  *centpapiers\.com* )
    site='CentPapiers'
    title_pattern='.*<title>(.*) . CentPapiers</title>'
    author_pattern='\s*<li class="saface"><h2><a href=".*" title=".*" rel="author">(.*)</a></h2></li>'
    date_pattern='<img src="/wp-content/themes/centpapiers/images/clock.gif"> (.*201.) '
    ;;

  *itrmobiles\.com* )
    site='ITRmobiles.com'
    author='la rédaction'
    date_pattern='<td class="date">(.*)</td>'
    ;;

  *agenceecofin\.com* )
    site='Agence Ecofin'
    title_pattern='.*<title>(.*) - .*</title>'
    author='la rédaction'
    date_pattern='(.* 201.) ..:..		</span>'
    ;;

  *rebellyon\.info* )
    site='RebelLyon.info'
    author='la rédaction'
    date_pattern='<p class="info-publi"><abbr class="published" title=".*">Publi&eacute; le (.*)</abbr></p>'
    ;;

  *ginjfo\.com* )
    site='GinjFo'
    title_pattern='property="og:title" content="(.*)"/><meta'
    author_pattern='href=".*" title="">(.*)</a></span> <span>dans <a'
    date_pattern='property="article:published_time" content="(.*)T.*" /><meta'
    ;;

  *aqui\.fr* )
    site='Aqui!'
    recode l1..UTF-8 article.html
    title_pattern='.*<title>Cultures . (.*) - .*</title>'
    author_pattern='.*<div class="signature" align="justify">(.*) <br /></div>'
    date_pattern='<img src="http://www.aqui.fr/images/timeicon.gif" alt="" width="12" height="12" /> (.*)'
    ;;

  *echos-judiciaires\.com* )
    site='Echos judiciaires Girondins'
    recode l1..UTF-8 article.html
    title_pattern='.*<title>.* - (.*)</title>'
    author_pattern='^	([[:alpha:]]* [[:alpha:]\&\;]*)</p><br />'
    date_pattern='<meta name="Date-Revision-yyyymmdd" content="(.*)">'
    ;;

  *ceriseclub\.com* )
    site='CeriseClub'
    title_pattern='.*<title>(.*) - .*</title>'
    author_pattern='.*<div id="stats" class="clearfloat"><span class="left">Publié par (.*) le .*</span><span class="right"><a href="#respond">.* commentaires</a>'
    date_pattern='.*<div id="stats" class="clearfloat"><span class="left">Publié par .* le (.*) &#150; .*</span><span class="right"><a href="#respond">.* commentaires</a>'
    ;;

  *toulouse7\.com* )
    site='Toulouse7.com'
    title_pattern='.*<h2 id="innerPostTitle">(.*)</h2>'
    author_pattern='.*<span class="author">Ecrit par <a href=".*" title="Articles par .*">(.*)</a></span>'
    date_pattern='.*<span class="date">(.*)</span>'
    ;;

  *france-info\.com* )
    site='france info'
    title_pattern='.*<title>(.*) - .* - .*</title>'
    author_pattern='.*<span class="auteur">(.*) - .*</span></h2>'
    date_pattern='.*<span class="auteur">.* - (.*)</span></h2>'
    ;;

  *weka\.fr* )
    site='Weka'
    title_pattern='<title>(.*) - .*</title>'
    author='la rédaction'
    date_pattern='<span class="date-publication">(.* 201.) '
    ;;

  *lagazettedescommunes\.com* )
    site='La gazette.fr'
    title_pattern='<title>(.*) - .*</title>'
    author_pattern='<p>Par (.*)</p>'
    date_pattern='Publié le (.*201.).*</span></p>'
    ;;

  *scinfolex\.wordpress\.com* )
    site=':: S.I.Lex ::'
    title_pattern='<title>(.*) . :: S.I.Lex ::</title>'
    author_pattern='<span class="meta-prep meta-prep-author">Publié le</span> <a href=".*"><span class="entry-date">.*</span></a> <span class="meta-sep">par</span> <span class="author vcard"><a class="url fn n" href=".*" title=".*">(.*)</a></span>					</div><!-- .entry-meta -->'
    date_pattern='<span class="meta-prep meta-prep-author">Publié le</span> <a href=".*" title=".*" rel="bookmark"><span class="entry-date">(.*)</span></a> <span class="meta-sep">par</span> <span class="author vcard"><a class="url fn n" href=".*" title=".*">.*</a></span>					</div><!-- .entry-meta -->'
    ;;

  *tunisiabuzz\.com* )
    site='TunisiaBuzz'
    title_pattern='<title>(.*) . TunisiaBuzz</title>'
    author_pattern=''
    date_pattern=''
    ;;

  *greenetvert\.fr* )
    site='Green et Vert'
    title_pattern='<title>(.*) . Green et Vert</title>'
    author_pattern=''
    date_pattern=''
    ;;

  *elwatan\.com* )
    site='El Watan'
    title_pattern='<title>(.*) - Actu Centre - El Watan</title>'
    author_pattern='<h5 class="signature">(.*)</h5>'
    date_pattern='<span class="date-publi">le (.*)&nbsp;.&nbsp;.*</span><br />'
    abstract_pattern='(.*)</p></h2>'
    ;;

  *cosmopolitan\.fr* )
    site='Cosmopolitan.fr'
    title_pattern='<title>(.*) - Cosmopolitan.fr</title>'
    author_pattern='<ul class="auteur_article"><li>(.*)</li></ul>											<p class="date-right">(.*) .*</p>'
    date_pattern='<ul class="auteur_article"><li>.*</li></ul>											<p class="date-right">(.*) .*</p>'
    abstract_pattern='<div class="collection_sousTitre"><h2> (.*) '
    ;;

  *voir\.ca* )
    site='Voir.ca'
    title_pattern='<title>.*&nbsp;: (.*) &ndash; .* &ndash; Voir.ca</title>'
    author_pattern='<span class="by-author">par</span> (.*)				</div>'
    date_pattern='<span>(.*) &middot; .*</span>'
    ;;

  *cfo-news\.com* )
    site='Finyear'
    author='la rédaction'
    date_pattern='<div class="access">(.* 201.)</div>'
    ;;

  *liberte-algerie\.com* )
    site='Liberté Algérie'
    title_pattern='<title>(.*) - .*</title>'
    author_pattern='<span class="auteur">Par : (.*)</span><!--<span class="lecture">Lu .*</span>-->'
    date_pattern='<span class="edition">(.* 201.) .*</span>'
    ;;

  *rencontres-sociales\.org* )
    site='Rencontres Sociales'
    title_pattern='<title>(.*) - .*</title>'
    author_pattern='<p class="date-auteur">.* - (.*)</p>'
    date_pattern='<p class="date-auteur">(.*) - .*</p>'
    ;;

  *bulletins-electroniques\.com* )
    site='Bulletins Electroniques'
    author='la rédaction'
    date_pattern='<p><span class="style32">.*&nbsp; </span><span class="style55">&gt;&gt;</span><span class="style42">&nbsp;&nbsp;(.*)</span></p></td>'
    abstract_pattern='<p class="style96"><span class="style95">(.*)<br />'
    ;;

  *lavoixdunord\.fr* )
    site='La Voix du Nord'
    title_pattern='<title>(.*) - .* - .*</title>'
    author_pattern='.*<p>PAR (.*) <div'
    date_pattern='<meta name="published" content="(.*)"\s*/>'
    ;;

  *macbidouille\.com* )
    site='MacBidouille'
    title_pattern='<title>.* - .* - (.*)</title>'
    author_pattern='<div class="info">Par <a href=".*">(.*)</a>'
    date_pattern='- (.*201.) .* - .* - '
    ;;

  *tomsguide\.fr* )
    author_pattern="_gaq.push\(\['_setCustomVar', 2, 'Author', '(.*)', 3\]\);"
    date_pattern='.*<span class="date upper cInfos"><time datetime=".*" itemprop="datePublished">(.*) ..:..</time></span>'
    ;;

  *revioo\.com* )
    site='Revioo'
    recode l1..UTF-8 article.html
    author_pattern='.*Par (.*) le <a'
    date_pattern='.*le <a href=".*">(.*) &agrave; .*</a>'
    ;;

  *ludovia\.com* )
    title='Numérique éducatif : les politiques des collectivités locales passées au crible'
    author_pattern='<span>By <a href=".*">(.*)</a> on .*</span>'
    date_pattern='<span>By <a href=".*">.*</a> on (.*)</span>'
    ;;

  *fredzone\.org* )
    site='FZN'
    author_pattern='<li.*>Par (.*)</li>'
    date_pattern='<li.*>le (.*) &agrave; .*</li>'
    ;;

  *zevillage\.net* )
    site='ZeVillage.net'
    author_pattern='<span class="post-meta-author">Publié par:  <a href=".*" title="">(.*) </a></span>'
    date_pattern='<meta property="article:published_time" content="(.*)T.*" />'
    ;;

  *sciencepresse\.qc\.ca* )
    site='Agence Science-Presse'
    title_pattern='<title>(.*) . Agence Science-Presse</title>'
    author_pattern='<div class="author_info"><span class="author_info_name"><a href=".*">(.*)</a></span>, le .*, .*</div>'
    date_pattern='<div class="author_info"><span class="author_info_name"><a href=".*">.*</a></span>, le (.*), .*</div>'
    ;;

  *greenit\.fr* )
    site='GreenIT.fr'
    title_pattern='<title>(.*) . GreenIT.fr</title>'
    author_pattern='<div class="span-7"><p class="bottom"><strong>Par (.*)</strong>&nbsp;&nbsp;-&nbsp;&nbsp;.*</p></div>'
    date_pattern='<div class="span-7"><p class="bottom"><strong>Par .*</strong>&nbsp;&nbsp;-&nbsp;&nbsp;(.*)</p></div>'
    ;;

  *accessoweb\.com* )
    site='accessoweb'
    author_pattern='<div class="access">Publié par <a rel="author" class="liens" href=".*">(.*)</a>'
    date_pattern='le (.*)</div>'
    ;;

  *newsring\.fr* )
    site='Newsring'
    title_pattern='.* - (.*) - '
    author_pattern='class="name">(.*)</span> <span class="icon certified" title="Utilisateur certifié">&nbsp;</span></div>'
    date_pattern='<div id="stats" class="loading" data-url=".*"  data-date="(.*)" data-id=".*" ></div>'
    ;;

  *estrepublicain\.fr* )
    site='estrepublicain.fr'
    title_pattern='<title>France - Monde . (.*) - .*</title>'
    author='la rédaction'
    date_pattern='(.*) &#224; ..h..	'
    ;;

  *levif\.be* )
    site='Le Vif.be'
    title_pattern='<title>(.*) - .* - .* - .*</title>'
    author='la rédaction'
    date_pattern='^(.* 201.) à ..h..$'
    ;;

  *marianne2\.fr* )
    site='Marianne 2'
    author_pattern='<meta name="author" lang="fr" content="(.*) - Marianne" />'
    date_pattern='<div class="access">(.*) à .* . Lu .* fois '
    ;;

  *marianne\.net* )
    site='Marianne'
    author_pattern='<meta name="author" lang="fr" content="(.*)" />'
    date_pattern='<div class="access">(.*) à .*</div>'
    ;;

  *cameroonvoice\.com* )
    site='Cameroonvoice'
    author_pattern='<p><strong>(.*)</strong></p></div>'
    date_pattern='<div class="update">(.*201.) .*</div>'
    ;;

  *challenges\.fr* )
    site='Challenges'
    title_pattern='.*<title>(.*) - .*</title>'
    author_pattern='.*<span itemprop="name">(.*)</span>'
    date_pattern='.*Créé le (..-..-201.) à '
    ;;

  *nelsondumais\.com* )
    site='La chronique de Nelson'
    title_pattern='<title>(.*) . La chronique de Nelson</title>'
    author_pattern='Publi.. par <a href=".*" title=".*" rel="author">(.*)</a>.*</a>.*</a>'
    date_pattern='.*le (.*) dans <a href=".*" title=".*" rel="category tag">.*</a> . <a href=".*" title=".*">.*</a>'
    ;;

  *lemainelibre\.fr* )
    site='lemainelibre.fr'
    title_pattern='<title>(.*) . .*</title>'
    author='la rédaction'
    date_pattern='(.*) - ..:..     					 </div>'
    ;;

  *tv5\.org* )
    site='TV5Monde'
    title_pattern='<title>TV5MONDE : (.*)</title>'
    author_pattern='.*<span class="info_signature">Par (.*)</span></span>.*</span></span>.*</span></span>.*</span></span>.*</span></span>.*</span></span>'
    date_pattern='.*<span class="info_wrap"><span class="info_date">(.*)</span><span class="info_signature">'
    ;;

  *france24\.com* )
    site='France 24'
    title_pattern='<title>(.*) - .*</title>'
    author='la rédaction'
    date_pattern='(.*) - ..H..&nbsp;&nbsp;'
    ;;

  *place-publique\.fr* )
    site='Place Publique'
    title_pattern='<title>.* - (.*)</title>'
    author_pattern='<h4 class="crayon article-soustitre-.*">(.*)</h4>'
    date='vendredi 5 octobre 2012'
    ;;

  *vonews\.fr* )
    site='Vonews'
    recode l1..UTF-8 article.html
    title_pattern='<div class="entry-title"><b>(.*)</b><br></div><!--titre -->'
    author='la rédaction'
    date_pattern='<div class="date"><strong>.(.*) .*</strong></div><br /><!--date -->'
    ;;

  *tdg\.ch* )
    site='Tribune de Genève'
    title_pattern='<title>(.*) - .* - .*</title>'
    author_pattern='<h5>Par (.*). Mis à jour le .* <a href=".*">.* Commentaires</a></h5>'
    date_pattern='<h5>Par .* Mis à jour le (.*) <a href=".*">.* Commentaires</a></h5>'
    ;;

  *lci\.tf1\.fr* )
    site='TF1 News'
    title_pattern='.*<title>(.*) - .* - .*</title>'
    author_pattern='.*par <a href=".*" class=".*" rel="author" itemprop="author">(.*)</a><br> le <meta itemprop="datePublished" content=".*">.* à .*'
    date_pattern='.*par <a href=".*" class=".*" rel="author" itemprop="author">.*</a><br> le <meta itemprop="datePublished" content=".*">(.*) à .*'
    ;;

  *lesaffaires\.com* )
    site='LesAffaires.com'
    title_pattern='.*<title>(.*) - .*</title>'
    author_pattern='<a href="/auteur/.*">(.*)</a>'
    date_pattern='(.*-201.)\s*<span class=.*> .modifi&eacute; le .* &agrave; .*.</span>'
    ;;

  *ariegenews\.com* )
    site='ariegenews.com'
    title_pattern='.*<title>(.*) - .*</title>'
    author='la rédaction'
    date_pattern='<div class="box_tools_left">(.*) . ..:..</div>'
    ;;

  *infotchad\.com* )
    site='Infotchad.com'
    recode l1..UTF-8 article.html
    title_pattern='<h1>(.*)  </h1>'
    author_pattern='.*<br><br><br>(.*)<br></p>'
    date='mardi 20 mars 2012'
    ;;

  *sur-la-toile\.com* )
    site='Sur la Toile'
    recode l1..UTF-8 article.html
    title_pattern='.*<title>(.*)</title>'
    author_pattern='.*Article rédigé par (.*)"> <a'
    date_pattern='.*title="Publié le (.*201.)"> '
    ;;

  *bastamag\.net* )
    title_pattern='<meta property="og:title" content="(.*)" />'
    author_pattern='<p class="publication"><span class="authors">par  <span class="vcard author" itemprop=.author.><a class="url fn spip_in" href=".*">(.*)</a></span></span><time pubdate="pubdate" datetime=".*" itemprop="datePublished">.*</time></p>'
    date_pattern='<p class="publication"><span class="authors">par  <span class="vcard author" itemprop=.author.><a class="url fn spip_in" href=".*">.*</a></span></span><time pubdate="pubdate" datetime=".*" itemprop="datePublished">(.*)</time></p>'
    ;;

  *essonneinfo\.fr* )
    site='Essonne Info'
    title_pattern='<title>(.*) . Essonne Info .*</title>'
    author_pattern='.* Auteur <a href=".*" title="Articles par .*" rel="author">(.*)</a>   '
    date_pattern='(.*201.) . Auteur <a href=".*" title="Articles par .*" rel="author">(.*)</a>   '
    ;;

  *decideo\.fr* )
    site='Decideo.fr'
    author_pattern='<meta name="author" lang=".*" content="(.*)" />'
    date_pattern='<div class="access">(.*)</div>'
    ;;

  *journaldugeek\.com* )
    site='Le Journal du Geek'
    title_pattern='.*<title.*>(.*)</title>'
    author_pattern='href="/member.*" title=".*">(.*)</a>, .* &agrave; .* <span><a'
    date_pattern='href="/member.*" title=".*">.*</a>, (.*) &agrave; .* <span><a'
    ;;

  *investir-en-tunisie\.net* )
    site='Investir En Tunisie'
    title_pattern='.*<title.*>(.*)</title>'
    author_pattern='By		(.*)		</span>'
    date_pattern='(.* 201.) ..:..		</span>'
    ;;

  *letraitdunion\.com* )
    site="Le Trait d'Union"
    title_pattern='<title.*>(.*) - .* - .*</title>'
    author_pattern='<a href="/Auteur-.*">(.*)</a>'
    date_pattern='<span class="published" title=".*">(.*)</span>'
    ;;

  *lanouvellerepublique\.fr* )
    site='Nouvelle République'
    author_pattern='^					(\w* \w*)</span>'
    author='la rédaction'
    date_pattern='					<link rel="canonical" href=".*n/Contenus/Articles/(.*)/\w'
    ;;

  *mlactu\.fr* )
    site='ML actu'
    title_pattern='<title.*>(.*) . MLactu</title>'
    author_pattern='(\w* \w*)</p>'
    date_pattern='<span class="submitted">Publi&eacute; le (.*)</span>'
    ;;

  *lejdd\.fr* )
    site='leJDD.fr'
    title_pattern='<title.*>(.*) . leJDD.fr</title>'
    author_pattern='<p><strong>(.*) - .*</strong></p>'
    date_pattern='</span>(.*)'
    ;;

  *mondialisation\.ca* )
    site='Mondialisation.ca'
    recode l1..UTF-8 article.html
    title_pattern='<div class="articleTitle">(.*)</div>'
    author_pattern='<div class="articleAuthorName">par    (.*)</div><br></td>'
    date_pattern='<td colspan="2" align="left" nowrap><div class="bigArticleText12"><a href="http://www.mondialisation.ca">Mondialisation.ca</a>, Le (.*)</div></td>'
    title='Les États-Unis persistent et signent: Nous continuerons à espionner'
    author='Chems Eddine Chitour'
    date='jeudi 23 janvier 2014'
    ;;

  *arte\.tv* )
    site='Arte'
    title_pattern='<title.*>(.*)</title>'
    date_pattern='<strong>ARTE Journal</strong> - (.*)'
    ;;

  *meta-media\.fr* )
    site='Metamedia'
    title_pattern='<title.*>(.*) . Metamedia</title>'
    author_pattern='<h4><span class="category"><a href=".*" title=".*" rel="category tag">.*</a></span><span class="date">.*</span><span class="by"> by <a href=".*" title="Articles par .*">(.*)</a> </span></h4>'
    date_pattern='<h4><span class="category"><a href=".*" title=".*" rel="category tag">.*</a></span><span class="date">(.*)</span><span class="by"> by <a href="http://meta-media.fr/author/metamedia/" title="Articles par .*">.*</a> </span></h4>'
    ;;

  *hauteprovenceinfo\.com* )
    site='Haute-Provence Info'
    author='la rédaction'
    date_pattern='</div><br/><div class=articleSignature>(.*) .*</div><div>'
    ;;

  *maxisciences\.com* )
    site='Maxisciences'
    recode l1..UTF-8 article.html
    author='la rédaction'
    date_pattern='<div id="gauche">Info r&eacute;daction, publi&eacute;e le (.*)</div>'
    ;;

  *fr\.euronews\.com* )
    site='euronews'
    title_pattern='<title.*>(.*) . euronews, .*</title>'
    author='la rédaction'
    date_pattern='<p class="cet">(.*) .* CET</p>'
    ;;

  *maghrebemergent\.info* )
    site='Maghreb Emergent'
    author='la rédaction'
    date_pattern='(.* 201.) ..:..			                  </li>'
    ;;

  *news\.cnet\.com* )
    site='CNET News'
    title_pattern='.*<title.*>(.*) . Internet & Media - .*</title>'
    author_pattern='.*<div class="postByline"> <a rel="author" href=".*"><figure><img class="mugshot" alt=".*" height="43" width="60" src=".*" /></figure></a> <div id="nameAndTime"> <span class="author"> <div class="singleAuthor"> by <a rel="author" href=".*">(.*)</a> </div> </span> '
    date_pattern='.*<time class="datestamp"> (.*) </time>'
    ;;

  *journaldequebec\.com* )
    site='Le Journal de Québec'
    title_pattern='<meta property="og:title" content="(.*)"/>'
    author_pattern='<h2>(.*) / Agence QMI</h2>'
    date_pattern='<time datetime="(.*)T.*" pubdate class="published dtreviewed value-title">'
    ;;

  *ingenieurduson\.com* )
    site='Ingénieur du son'
    title='Acta: Bientôt lettre morte?'
    author='Luciana Ferreira'
    date='samedi 9 juin 2012'
    ;;

  *lejsd\.com* )
    site='Le Journal de Saint-Denis'
    title_pattern='.*<title.*>(.*) . Le Journal de Saint-Denis</title>'
    author='Marylène Lenfant'
    date_pattern='.*<span class="date">Publié le (.*) ..:..'
    ;;

  *lesoleil\.sn* )
    site='Le soleil'
    author_pattern='^<p style="text-align: right;"><strong><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;"> (.*) </span></strong></p>'
    date_pattern='(.*201.) ..:..				</span>'
    ;;

  *fqde\.qc\.ca* )
    site='FQDE'
    title_pattern='Fédération québécoise des directions d&#8217;établissement d&#8217;enseignement&nbsp;.&nbsp;  (.*)&#8230;			    </title>'
    author_pattern='^<p style="text-align: left;"><strong>(.*)<br />'
    date='mercredi 13 juin 2012'
    ;;

  *gossy\.fr* )
    site='Gossy'
    author_pattern='<span class="ilya">Le .* par <a href=".*" class="color_gris_333333">(.*)</a></span> <a href="#zone_commentaire"><span class="mini_bulle"></span><span class="commentaire_nombre">.*</span></a>'
    date_pattern='<span class="ilya">Le (.*) à .* par <a href=".*" class="color_gris_333333">.*</a></span> <a href="#zone_commentaire"><span class="mini_bulle"></span><span class="commentaire_nombre">.*</span></a>'
    ;;

  *mondediplo.net* )
    site='Les blogs du Diplo'
    author_pattern='<p class="date"><abbr class="published" title=".*">.*</abbr>, par  (.*)</p>'
    date_pattern='<p class="date"><abbr class="published" title=".*">(.*)</abbr>, par  .*</p>'
    ;;

  *computerworlduk\.com* )
    site='ComputerworldUK'
    title_pattern='<title>(.*) - .* - ComputerworldUK.com</title>'
    author_pattern='<h3 class="blogAuthor"><a href=".*">(.*)</a></h3>'
    date_pattern='<p class="articleInfo">By <span itemprop="author"><a href=".*" title=".*" rel="author">.*</a></span> . Published .*<span itemprop="datePublished">(.*)</span></p>'
    ;;

  *madmoizelle\.com* )
    site='madmoiZelle'
    author_pattern='<p>Pondu  par (.*) le (.*)                        &nbsp;'
    date_pattern='<p>Pondu  par .* le (.*)                        &nbsp;'
    ;;

  *smartplanet\.fr* )
    site='SmartPlanet.fr'
    title_pattern='<title.*>(.*) . SmartPlanet.fr</title>'
    author_pattern='<p><em>par (.*)</em></p>'
    date_pattern='<p class="meta">Par la rédaction . (.* 201.) . <a href="#comments" class="icon i-comment comments"><b>.*</b> commentaire</a></p>'
    ;;

  *macplus\.net* )
    site='MacPlus'
    recode l1..UTF-8 article.html
    title_pattern='.*<title.*>(.*)</title>'
    author='la rédaction'
    date='5 juillet 2012'
    ;;

  *huffingtonpost* )
    author_pattern='<meta name="author" content="(.*)" />'
    date_pattern='<meta name="publish_date" content="(.*)" />'
    date_pattern='<span class="arial_11 color_696969">Publication: (.*) .*</span>'
    ;;

  *cdeacf\.ca* )
    site='Le CDÉACF'
    title_pattern='<title.*>(.*) . Le CDÉACF</title>'
    author_pattern='<p class="submitted">Soumis par (.*) le .*</p>'
    date_pattern='<p class="submitted">Soumis par .* le (.*) - .*</p>'
    ;;

  *novopress\.info* )
    site='Novopress.info'
    title_pattern='(.*)&nbsp;.&nbsp;:: Novopress.info'
    author='la rédaction'
    date_pattern='<meta name="description" content="(.*) – '
    ;;

  *futura-sciences\.com* )
    recode l1..UTF-8 article.html
    title_pattern='<h1><div class="tailletexte">(.*)</div></h1>'
    author_pattern='<div class="auteur"><div class="tailletexte">Par (.*)'
    date_pattern='<div class="auteur_type">Le (.*) à .*</div>'
    ;;

  *lapresse\.ca* )
    title_pattern='<title>(.*) . \w* \w* . \w*</title>'
    author_pattern='<title>.* . (\w* \w*) . \w*</title>'
    date_pattern='<p><script language="Javascript">document.write.publicatioin_date."(.*) .*", ".*"..;</script>'
    ;;

  *quebecoislibre\.org* )
    site='Le Québécois Libre'
    recode l1..UTF-8 article.html
    date_pattern='<p class="texte8">Montréal, (.*)<span style="font-weight: 400">'
    ;;

  *courrierinternational\.com* )
    site='Courrier international'
    title_pattern='<title.*>(.*) . Courrier international</title>'
    author_pattern='<p class="infos"><span class="date">.*</span>&emsp;.&emsp;(.*)&emsp;.&emsp;<a href=".*">.*</a></p>'
    date_pattern='<p class="infos"><span class="date">(.*)</span>&emsp;.&emsp;.*&emsp;.&emsp;<a href=".*">.*</a></p>'
    ;;

  *visionary\.wordpress\.com* )
    site='Marketing & Innovation'
    author_pattern='<span class="author vcard">By <a class="url fn n" href=".*" title=".*">(.*)</a></span>				</div>'
    date_pattern='<span>(.* 201.)</span>'
    ;;

  *cafebabel\.fr* )
    site='Cafebabel'
    author_pattern='<dt><a href=".*"><span class="avatar"><img src="http://static2.cafebabel.com/avatar/.*/50x50/avatar.jpg" alt=".*" height="50" width="50" /></span>\s*(.*)</a></dt>'
    date_pattern='(../../1.)'
    ;;

  *infogm\.org* )
    site="Inf'OGM"
    title_pattern='<title.*>.* - (.*)</title>'
    author_pattern='<font class="auteur_date">par <span class="vcard author"><a class="url fn spip_in" href=".*">(.*)</a></span>, '
    date_pattern='<span class="vcard author"><a class="url fn spip_in" href=".*">.*</a></span> , (.*)</font>'
    ;;

  *web-tech\.fr* )
    site='Web & Tech'
    title_pattern='^<title>(.*) . Web .* Tech .*</title>'
    author_pattern='Publié par <a href="http://web-tech.fr/author/.*">(.*)</a> le '
    date_pattern='Publié par <a href="http://web-tech.fr/author/.*">.*</a> le (.*).<br />'
    ;;

  *invention-europe\.com* )
    site='Invention - Europe'
    title_pattern='^<title>(.*) . Invention - Europe .*</title>'
    author_pattern='<td><span class="pn-sub">Publié par : (.*), le .* - .* <br >'
    date_pattern='<td><span class="pn-sub">Publié par : .*, le (.*) - .* <br >'
    ;;

  *europe1\.fr* )
    site='Europe1.fr'
    title_pattern='<title>(.*) - Europe1.fr.*</title>'
    author_pattern='<p class="author">Par <strong>(.*)</strong></p>'
    date_pattern='<span>Publi&eacute; le (.*) &agrave; .*</span>'
    ;;

  *computerworld\.com* )
    site='Computerworld'
    title_pattern='<title>(.*) - Computerworld</title>'
    date_pattern='<div id="date">(.*) ..:...*</div>'
    ;;

  *thd\.tn* )
    site='Tunisie haut débit'
    title_pattern='.*<meta name="title" content="(.*)" /><meta name="author" content=".*" /><meta name="description"'
    author_pattern='.*<meta name="author" content="(.*)" /><meta name="description"'
    date='mardi 2 octobre 2012'
    ;;

  *tourmag\.com* )
    site='TourMaG.com'
    author_pattern='<h2 class="soustitre">Lire la chronique de (.*)</h2>'
    date_pattern='<div class="access">Rédigé par .* le (.*)</div>'
    ;;

  *wsj\.com* )
    site='WSJ.com'
    author_pattern='<!-- article start --><ul class="socialByline"><!-- author.s. --><li>By </li> <li class="popC byName popClosed"><a class=.popTrigger. href=.* onclick=.event.preventDefault...>(.*)</a><div onclick="event.stopPropagation.."><div class="popBox connectBox">'
    date_pattern='.*<li class="postStamp">(.*)</li>.*<li class="posterName">.*'
    ;;

  *blogspot\.co\.uk* )
    site='The IPKat'
    author_pattern='^<span class=.fn.>(.*)</span>'
    date_pattern='^<h2 class=.date-header.><span>(.*)</span></h2>'
    ;;

  *infobourg\.com* )
    site='Infobourg.com'
    title_pattern='<title>\s*(.*) . Infobourg.com.*</title>'
    author_pattern='<strong><a href=".*" title="Articles par .*" rel="author">(.*)</a></strong>, publi&eacute; le .*'
    date_pattern='<strong><a href=".*" title="Articles par .*" rel="author">.*</a></strong>, publi&eacute; le (.*)'
    ;;

  *directgestion\.com* )
    site='Directgestion'
    date_pattern='(.*)					</p>'
    ;;

  *village-justice\.com* )
    site='Village de la Justice'
    title_pattern='<title>(.*) Par .*</title>'
    author_pattern='<p class="label-auteur"> Par (.*), .*</p>'
    date_pattern='<span class="date pull-right">- (.*)</span>'
    ;;

  *enviscope\.com* )
    site='enviscope.com'
    author_pattern='.*<span class="CHAPEAU">(.*)&nbsp; . &nbsp;<span class="DATECREATION">.* - .*</span></span></div>'
    date_pattern='.*<span class="CHAPEAU">.*&nbsp; . &nbsp;<span class="DATECREATION">(.*) - .*</span></span></div>'
    ;;

  *techworld\.com* )
    site='Techworld'
    author_pattern='<li id="article_author"><a rel="author" href="/author/.*">(.*)</a> .*</li>'
    date_pattern='<li id="article_date">(.*) ..:..</li>'
    ;;

  *carredinfo\.fr* )
    site="Carré d'info"
    title_pattern='<title.*>\s*(.*) . Carré.*\s*</title>'
    author_pattern='<a href="http://carredinfo.fr/auteurs/" class="meta-author">(.*)</a>'
    date_pattern='<time class="meta-date">(.*)</time>'
    ;;

  *agefi\.com* )
    site='Agefi.com'
    title_pattern="<title.*>\s*(.*)\s* . $site</title>.*"
    author_pattern='<p class="author">(.*)<br /></p>'
    date_pattern='<div class="news-single-timedata">(.*)\s*</div>'
    ;;

  *associationmodeemploi\.fr* )
    site="Association mode d'emploi"
    recode l1..UTF-8 article.html
    title_pattern='<title>(.*) . Association mode d.emploi</title>'
    date_pattern='.*Article du num&eacute;ro .* - (.*) - Les logiciels libres'
    ;;

  *bbc\.co\.uk* )
    site='BBC News'
    title_pattern='<title>.* - (.*)</title>'
    author='la rédaction'
    date_pattern='<span class="date">(.*)</span>'
    ;;

  *guardian\.co\.uk* )
    site='The Guardian'
    title_pattern='<meta property="og:title" content="(.*)"/>'
    author_pattern='<span itemscope itemprop="author" itemtype="http://schema.org/Person"><span itemprop="name"><a class="contributor" rel="author" itemprop="url" href=".*">(.*)</a></span></span>	</div>'
    date_pattern='<time itemprop="datePublished" datetime=".*" pubdate>(.*) ..\... .*</time>	        '
    ;;

  *krinein\.com* )
    site='Krinein'
    author_pattern='^<p style="text-align:right;"><a href=".*" style=".*">(.*)</a></p></div>'
    date_pattern='^par <a rel="author" href=".*"><span class="reviewer">.*</span></a> - le <span class="dtreviewed">(.*)<span class="value-title" title=".*"></span></span></span>'
    ;;

  *eurojuris\.fr* )
    site='Eurojuris'
    author_pattern='^<a href="/fre/fiches/membres/membre.html.id=.*">(.*)</a><br />'
    date_pattern='^<p><i>Publié le (.*)<span id="includeLecteur"></span>'
    ;;

  *agencebretagnepresse\.com* )
    site='Agence Bretagne Presse'
    date_pattern='.*<div class="cartouche">Chronique du (.*) ..:.. de <b>'
    ;;

  *bfmtv\.com* )
    site='BFMtv'
    author='Olivier Laffargue'
    date_pattern='Le (.*) à '
    ;;

  *france3\.fr* )
    author_pattern='^<meta about=".*" property="foaf:name" content="(.*)" />'
    date_pattern='.*<li class="last">Publié le (.*201.).*, mis à jour le .*</li>'
    ;;

  *la-clau\.net* )
    site='la clau'
    author='la rédaction'
    date_pattern='<span class="data">(.*)\. .* </span>'
    ;;

  *europa\.eu* )
    site='Joinup'
    title_pattern='<title>(.*) . Joinup</title>'
    author_pattern='<div class="field field-submitted">Submitted by <a href=".*">(.*)</a> on .*</div>'
    date_pattern='<div class="field field-submitted">Submitted by <a href=".*">.*</a> on (.*)</div>'
    ;;

  *acrimed\.org* )
    site='Acrimed'
    title_pattern='<title>(.*) - Acrimed .*</title>'
    author_pattern='<p class="auteur">par  <span class="vcard author"><a class="url fn spip_in" href=".*">(.*)</a></span>, <abbr class="published" title=".*">le&nbsp;.*</abbr></p>'
    date_pattern='<p class="auteur">par  <span class="vcard author"><a class="url fn spip_in" href=".*">.*</a></span>, <abbr class="published" title=".*">le&nbsp;(.*)</abbr></p>'
    ;;

  *journalmetro\.com* )
    site='Métro'
    title_pattern='_gaq.push..._setCustomVar., 5, .Title. , .(.*)., 3 ..;'
    author_pattern='</a>Par 				    	<a itemprop="url" href="http://journalmetro.com/author/.*">(.*)</a>'
    date_pattern='<span class="post-date">(.*)</span>'
    ;;

  *ouishare\.net* )
    site='OuiShare'
    author_pattern='<span class="author"><span> On <b>.*</b></span><i> by </i><b>(.*)</b></span>'
    date_pattern='<span class="author"><span> On <b>(.*)</b></span><i> by </i><b.*</b></span>'
    ;;

  *lepetiteconomiste.com* )
    site='lepetiteconomiste.com'
    author='la rédaction'
    date_pattern='<div class="span-7 colborder"> (.*) </div>'
    ;;

  *lematin\.ch* )
    site='Le Matin'
    author_pattern='<h5>Par <a href="/stichwort/autor/.*">(.*)</a>.* Mis à jour le .* <a href=".*">.*</a></h5>'
    date_pattern='<h5>Par <a href="/stichwort/autor/.*">.*</a>.* Mis à jour le (.*) <a href=".*">.*</a></h5>'
    ;;

  *ecoloinfo\.com* )
    site='Ecolo-Info'
    author_pattern='^<h4>Le .* Par <a href="http://www.ecoloinfo.com/author/.*/" title="Articles par .*" rel="author">(.*)</a></h4>'
    date_pattern='^<h4>Le (.*) . Par <a href="http://www.ecoloinfo.com/author/.*/" title="Articles par .*" rel="author">Anne-Sophie</a></h4>'
    ;;

  *abidjan\.net* )
    author='le democrate'
    date='mercredi 6 mars 2013'
    author_pattern='^<div class=FontArticleSource>Publi? le .* &nbsp;.&nbsp; <strong> (.*)</strong><br /></div>'
    date_pattern='^<div class=FontArticleSource>Publi? le (.*) &nbsp;.&nbsp; <strong> .*</strong><br /></div>'
    ;;

  *leral\.net* )
    site='leral.net'
    author='la rédaction'
    date_pattern='<div class="access">(.*) - .*</div>'
    ;;

  *radio-canada\.ca* )
    site='Radio-Canada.ca'
    title_pattern='<title>(.*) . ICI.Radio-Canada.ca</title>'
    author='la rédaction'
    date_pattern='<meta content="(.*)....h.*" name="dc.date.created" />'
    ;;

  *lesnews\.ca* )
    site='LesNews'
    author_pattern='.*&nbsp;&agrave;&nbsp;..:.. &nbsp;&nbsp;.&nbsp; (.*) '
    date_pattern='(.*)&nbsp;&agrave;&nbsp;..:.. &nbsp;&nbsp;.&nbsp; .* '
    ;;

  *tradingsat\.com* )
    site='Trading Sat'
    author_pattern='<p class="copyright">(.*) - &copy;2013 Tradingsat.com</p>'
    date_pattern='(.*)&nbsp;&agrave;&nbsp;..:.. &nbsp;&nbsp;.&nbsp; .* '
    ;;

  *opensource\.com* )
    site='opensource.com'
    title_pattern="<title.*>\s*(.*)\s* . $site</title>.*"
    author_pattern='<span class="submitted">Posted .* by <a href=".*" title=".*">(.*)</a> <span class=".*">.*</span><a href=".*" class="article-author-feed"></a></span>'
    date_pattern='<span class="submitted">Posted (.*) by <a href=".*" title=".*">.*</a> <span class=".*">.*</span><a href=".*" class="article-author-feed"></a></span>'
    ;;

  *levinvinteur\.com* )
    site='Le Vinvinteur'
    title_pattern="<title.*>\s*(.*)\s* . $site</title>.*"
    author='la rédaction'
    date_pattern='le (.*201.)\s*<span class="separator">.</span>'
    ;;

  *egaliteetreconciliation\.fr* )
    site='Egalite et Réconciliation'
    title_pattern="<title.*>\s*(.*)\s* . $site</title>.*"
    author='la rédaction'
    date_pattern='<li>Publié le : <abbr class="published" title=".*">(.*)</abbr></li>'
    ;;

  *youphil\.com* )
    title_pattern="<title.*>\s*(.*)\s* . YOUPHIL</title>.*"
    author='la rédaction'
    date_pattern='</ul> . (../../20..)          </div>'
    ;;

  *lbr\.ca* )
    site='LBR.ca'
    author_pattern='<p><strong>&nbsp;(.*)</strong></p>'
    date_pattern='<tr><td>Publi&eacute; le <strong>(.*)</strong> &agrave; <strong>.*</strong></td>'
    ;;

  *afjv\.com* )
    site='AFJV'
    author_pattern='.*<h2>(.*)</h2>'
    date_pattern='.*&eacute;dition du (.* 201.)'
    ;;

  *itrgames\.com* )
    site='ITRgames'
    author='la rédaction'
    date_pattern='<div id="message">Publi&eacute; le (.*)</div>'
    ;;

  *tahiti-infos\.com* )
    author='la rédaction'
    date_pattern='<div class="access">Rédigé par .* le (.*) à .*</div>'
    ;;

  *blog-nouvelles-technologies\.fr* )
    site='BlogNT'
    title_pattern='(.*)-'
    author_pattern='<span>par</span> (.*) <span>le</span> .* <span>dans</span> <a href=".*" title=".*" rel="category tag">.*</a>, <a href=".*" title=".*" rel=".*">.*</a> <span>'
    date_pattern='<span>par</span> .* <span>le</span> (.*) <span>dans</span> <a href=".*" title=".*" rel="category tag">.*</a>, <a href=".*" title=".*" rel=".*">.*</a> <span>'
    ;;

  *itechpost\.com* )
    site='iTech Post'
    author='la rédaction'
    date_pattern='<div class="posted-time">First Posted: (.*)</div><span class="clearfix"></span>'
    ;;

  *lavieeco\.com* )
    site='La Vie éco'
    date_pattern='<p class="datepub" >(.*)</p>'
    ;;

  *ekonomico\.fr* )
    title_pattern="<title.*>(.*) . Ekonomico</title>.*"
    date_pattern='.*<p class="entry-date"><abbr class="published" title=".*">(.*)</abbr></p>'
    author='la rédaction'
    ;;

  *usine-digitale\.fr* )
    date_pattern='<time itemprop="datePublished" datetime=".*">Publié le (.*), &agrave; .*</time>'
    author_pattern='<p class="artAuteurDate">Par <a href=".*" rel="author" itemprop="author">(.*)</a>.*'
    ;;

  *geeko\.lesoir\.be* )
    site='Geeko'
    title_pattern='<title.*>\s*(.*)\s* . Geeko</title>'
    date_pattern='<div class="articleinfo">posté  par .* le (.*) dans'
    author_pattern='^<p><strong>(\w* \w*) \(.*\)</strong></p>'
    ;;

  *lesoir\.be* )
    title_pattern='<title>(.*) . Régions - lesoir.be</title>'
    date_pattern='<time><span id=".*" class="prettydate">(.*), .*<abbr title="heure">h</abbr>.*</span></time>   '
    author_pattern='<strong> (.*)</strong><br/>'
    ;;

  *libertalia\.org* )
    site='Libertalia'
    title_pattern='.*<title.*>(.*)</title>'
    date_pattern='class="item" id="post-8528"><h1 class="crayon article-surtitre.*">.*&nbsp;: l&#.*</h1><p>Publié le (.*) à .*, par <a'
    author_pattern='^href="/auteur/8">(.*)</a></p><div'
    ;;

  *archimag\.com* )
    site='Archimag'
    title_pattern='.*<title.*>(.*) . Archimag</title>'
    date_pattern='<span><i class="icon-calendar"></i> Le (.*)</span>'
    author_pattern='<span class="post-meta-user"><i class="icon-user"></i>  <a href=".*">(.*)</a> </span>'
    ;;

  *rtbf\.be* )
    date_pattern='<p class="about printable order-10"><span class="cat2"><a href=".*">REGIONS</a></span> . <span class="date updatedNews">Mis à jour le (.*) à .*</span></p>'
    author_pattern='^<p><strong>(.*)</strong></p></div>'
    ;;

  *tela-botanica\.org* )
    site='Tela Botanica'
    recode l1..UTF-8 article.html
    date_pattern='<p class="date">Mis en ligne (.*)'
    ;;

  *touleco\.fr* )
    date_pattern='<span class="date-publi">Publié le (.*) à .*</span> '
    author_pattern='<span class="auteurs">par <a href=".*">(.*)</a></span>'
    ;;

  *jolpress\.com* )
    site='JOL Press'
    title_pattern='<meta property="og:title" content="(.*)" />'
    date_pattern='publié le (.*201.)\s*</span>'
    author_pattern='Ecrit par <strong>(.*)</strong>'
    ;;

  *pressenza\.com* )
    site='Pressenza'
    date_pattern='<span class="meta_date">Date de publication:  <strong>(.*)</strong></span>'
    author_pattern='<p><strong>Par (.*)</strong></p>'
    ;;

  *techmissus\.com* )
    site='TechMissus'
    title_pattern='<meta property=.og:title. content=.(.*). />'
    date_pattern='<span class="date">(.*)</span>'
    author_pattern='<span class="author"><a href=".*" title=".*" rel="author">(.*)</a></span>'
    ;;

  *iledefrance\.fr* )
    site='Région Île-de-France'
    title_pattern='<meta property=.og:title. content=.(.*). />'
    date_pattern='<div class="field-item even">(.* 201.)</div>'
    author_pattern='^          <div class="field-item even">(\w* \w*)</div>$'
    ;;

  *newswire\.ca* )
    site='CNW'
    title_pattern='<h1><span class="din">(.*)</span></h1>'
    date_pattern='<li class="release_date" title=".*"><span>(.* 201.) .*</span>'
    author_pattern='^          <div class="field-item even">(\w* \w*)</div>$'
    ;;

  *wired\.com* )
    site='Wired'
    title_pattern='<meta name="Title" content="(.*)" />'
    date_pattern='<meta name="DisplayDate" content="(.*)" />'
    author_pattern='<meta name="Author" content="(.*)" />'
    ;;

  *ulaval\.ca* )
    site='lefil'
    date_pattern='<div class="volume_edition">Volume .*, num&eacute;ro 6<span class="date">(.*)</span></div>'
    ;;

  *informatiquenews\.fr* )
    date_pattern='<div class="volume_edition">Volume .*, num&eacute;ro 6<span class="date">(.*)</span></div>'
    author_pattern='<p class="vcard author">Par <a href=".*" class="fn url" rel="author">(.*)</a> </p>'
    date_pattern='<strong class="date">le <time datetime=".*">(.*)</time></strong>'
    ;;

  *tsugi\.fr* )
    site='Tsugi'
    title_pattern='<title.*>\s*(.*)\s* . Tsugi</title>'
    date_pattern='<div class="volume_edition">Volume .*, num&eacute;ro 6<span class="date">(.*)</span></div>'
    author_pattern='<span class="author-wrapper">par <span rel="sioc:has_creator"><a href=".*" title="Voir le profil utilisateur." class="username" xml:lang="" about=".*" typeof="sioc:UserAccount" property="foaf:name" datatype="">(.*)</a></span></span>'
    date_pattern='Publié le <span property="dc:date dc:created" content=".*" datatype="xsd:dateTime">(.*)</span>'
    ;;

  *lavie\.fr* )
    site='La Vie'
    title_pattern='<title.*>(.*) - .* - .*</title>'
    date_pattern='<div class="volume_edition">Volume .*, num&eacute;ro 6<span class="date">(.*)</span></div>'
    author_pattern='<p class="metadata"><span class="author"><strong>Propos recueillis par (.*)</strong></span> <br>.*</p>'
    date_pattern='<p class="metadata"><span class="author"><strong>Propos recueillis par .*</strong></span> <br>Créé le (.*201.).*</p>'
    ;;

  *globalsecuritymag\.fr* )
    site='Global Security Mag'
    title_pattern='<title.*>(.*) - .*</title>'
    date_pattern='<div class="volume_edition">Volume .*, num&eacute;ro 6<span class="date">(.*)</span></div>'
    author_pattern='.*<a class="noir" href="mailto:.*">(.*)</a>'
    date='mercredi 23 octobre 2013'
    ;;

  *bilan\.ch* )
    date_pattern='<div class="meta-line"><span property="dc:date dc:created" content=".*" datatype="xsd:dateTime">(.*)</span></div>'
    author_pattern='<a href="/author-import/.*" typeof="skos:Concept" property="rdfs:label skos:prefLabel" datatype="">(.*)</a></strong>'
    ;;

  *ragemag\.fr* )
    title_pattern='RAGEMAG . (.*)	</title>'
    date_pattern='<div class="meta-line"><span property="dc:date dc:created" content=".*" datatype="xsd:dateTime">(.*)</span></div>'
    author_pattern='<a href="/author-import/.*" typeof="skos:Concept" property="rdfs:label skos:prefLabel" datatype="">(.*)</a></strong>'
    ;;

  *la-croix\.com* )
    site='la Croix'
    title_pattern='<title.*>(.*) . La-Croix.com</title>'
    date_pattern='<span>(.*) - .*<meta itemprop="datePublished" content=".*"/></span>'
    author_pattern='(.*)    </strong>'
    ;;

  *lessentiel\.lu* )
    date_pattern='<p>(.* 201.) ..:..; '
    ;;

  *mag14\.com* )
    site='Mag14'
    title_pattern='.*<title.*>\s*(.*)\s*</title>'
    date_pattern='.*<td valign="top" class="createdate">(.* 201.) ..:..	</td>'
    author_pattern='.*<p align="right"><strong>(.*)</strong></p><p>'
    ;;

  *techniques-ingenieur\.fr* )
    site="Techniques de l'Ingénieur"
    title_pattern='.*<title.*>\s*(.*) - .*</title>'
    date_pattern='(.* 201.) '
    author_pattern='.*Par <u><strong>(.*)</strong>'
    ;;

  *usatoday\.com* )
    site="USA Today"
    title_pattern='.*<title.*>(.*)</title>'
    date='vendredi 6 décembre 2013'
    author='The Associated Press'
    ;;

  *creanum\.fr* )
    site='Creanum'
    title_pattern='(.*) > Creanum'
    date_pattern='<div class="NewsDate">Par <a href=".*">.*</a> le (.*)</div>'
    author_pattern='<div class="NewsDate">Par <a href=".*">(.*)</a> le (.*)</div>'
    ;;

  *marchespublicspme\.com* )
    site='Marches-Publics-PME'
    title_pattern='.*<title>(.*) - Marches-Publics-PME</title>'
    date_pattern='.*<div class="news_date">Ajout.* le : <span>(.*)</span></div>'
    author='la rédaction'
    ;;

  *economist\.com* )
    title_pattern='<title.*>\s*(.*) . The Economist</title>'
    date_pattern='<time class="date-created">(.*)</time><span class="location"> by .*</aside>'
    author_pattern='<time class="date-created">.*</time><span class="location"> by (.*) . .* .*</aside>'
    ;;

  *presseurop\.eu* )
    title_pattern='<title.*>\s*(.*) . Presse.*</title>'
    date_pattern='<div class="blockpl">(.*)</div>'
    author_pattern='<a href=".*" rel="author">(.*)</a>'
    ;;

  *boingboing\.net* )
    title_pattern='<title.*>(.*) - .*</title>'
    date_pattern='<p class="byline permalink"><a href=".*" title="Posts by .*" rel="author">.*</a> at (.*)  </p>'
    author_pattern='<meta itemprop="author" content="(.*)" />'
    ;;

  *miroir-mag\.fr* )
    date_pattern='.*<span class="articleinfodate">(.* 201.)</span> '
    author_pattern='.*<span class="articleinfoauteur"><a href="http://www.miroir-mag.fr/author/.*" title=".*" rel="author">(.*)</a></span> '
    ;;

  *metronews\.fr* )
    site='metronews'
    title_pattern='.*<title>(.*) . metronews</title>'
    date_pattern='.*<span class="date-text">Créé : </span>(.*) .*</div><h1'
    author_pattern='_gaq.push..._setCustomVar., 4, "Author", "(.*)", 3..;'
    ;;

  *libertebonhomme\.fr* )
    site='Liberté Bonhomme Libre'
    date_pattern='<span class="pts normal">Dernière mise à jour : (.*) à .*</span>'
    author_pattern='<span class="left pls" data-role="author-name">(.*)</span>'
    ;;

  *afriquinfos\.com* )
    site='Afriquinfos'
    recode l1..UTF-8 article.html
    title_pattern='.*<title>(.*) . Afriquinfos</title>'
    date_pattern='<div class="fechanota">(.*201.) . .* </div>'
    author_pattern='.*title="Voir toutes les contributions de (.*)">.*</a>.*'
    ;;

  *linformatique\.org* )
    site="L'informatique"
    date_pattern='<meta property="article:published_time" content="(.*)T.*" />'
    author_pattern='<link rel="author" href=".*" title="(.*) sur Google." />.*'
    ;;

  *lemondenumerique\.com* )
    site='Le Monde Numerique'
    title_pattern='.*<title>(.*)</title>.*'
    date_pattern='Posté le (.*) à .* par .* dans.*'
    author_pattern='Posté le .* par (.*) dans.*'
    ;;

  *lecourrier-lecho\.fr* )
    site='Le Petit Courrier du Val de Loir'
    title_pattern='.*<title>(.*) - .*</title>.*'
    date_pattern='<span class="pts normal">Dernière mise à jour : (.*) à .*</span>'
    author_pattern='<span class="left pls" data-role="author-name">(.*)</span>'
    ;;

  *techno-car\.fr* )
    site='Techno-Car'
    title_pattern='<title>(.*) . Techno-Car</title>'
    date_pattern='(.* 201.)									</li>'
    author_pattern='<li class="entry-author">Par <a href=".*" title="Articles par .*" rel="author">(.*)</a></li>'
    ;;

  *lamontagne\.fr* )
    site='la montagne'
    title_pattern='<title>.* - (.*)</title>'
    date_pattern='(.*4) - ..h..'
    author_pattern='<p><span class="auteur">(.*)</span><br /></p></texte>'
    ;;

  *largeur\.com* )
    site='Largeur.com'
    date_pattern='<table cellpadding="0px" cellspacing="0px" border="0px" style="width:100%"><tr><td class="une_tag"><a href=".*">.*</a></td><td style="text-align:right;"><span class="une_tag">(.*)</span></td></tr></table>'
    author='la rédaction'
    ;;

  *revenuagricole\.fr* )
    date_pattern='Créé le (.*) ..:..	</dd>'
    ;;

  *lasemaine\.fr* )
    site='La Semaine'
    title_pattern='.*<title.*>\s*(.*)\s*</title>.*'
    author_pattern='<p class="auteur">Par (.*) &bull; Journaliste de La Semaine &bull; .* &agrave; .*</p>'
    date_pattern='<p class="auteur">Par .* &bull; Journaliste de La Semaine &bull; (.*) &agrave; .*</p>'
    ;;

  *radins\.com* )
    site='radins.com'
    recode l1..UTF-8 article.html
    title_pattern='.*<title.*>\s*(.*)\s*</title>.*'
    author_pattern='.*<span class="fs11 bold ttu" target="_blank" rel="author">Par (.*)</span></span></span> </p> </div> </div>.*'
    date_pattern='.*<div class="fcf lh14 bggr">Ajout. le</div> <div class="rok fs40">.*</div> <div class="fcf lh14 bggr">(.*)</div> </div> </div>.*'
    ;;

  *miroirsocial\.com* )
    site='Miroir Social'
    author_pattern='<!--	<span class="authorPost">par (.*)</span>-->'
    date_pattern='<div id="datePostAll"><p>.*<span> (.*)</span>'
    ;;

  *courrier-picard* )
    author_pattern='.*<span class="author vcard fn">(.*)</span>.*'
    date_pattern='.*<span class="updated dtstamp" title=".*">Publié le <span itemprop="datePublished">(.*)</span></span>.*'
    ;;

  *dynamique-mag* )
    author='la rédaction'
    date_pattern='.*<div class="date_parutionauteur" align="right">(.*)</div>.*'
    ;;

  *zonebourse* )
    site='zonebourse'
    recode l1..UTF-8 article.html
    title="Microsoft: L'Open Source ne fait pas recette en Algérie"
    author='la rédaction'
    date='mercredi 19 mars 2014'
    ;;

  *focusur* )
    author_pattern='<span class="by-author"> <span class="sep"> par </span> <span class="author vcard"><a class="url fn n" href=".*" title="Voir tous les articles de .*" rel="author">(.*)</a></span> dans </span>'
    date_pattern='<span class=".*" style="text-transform: none;">Publié le </span>(.*) à .*'
    ;;

  *rtve\.es* )
    site='rtve'
    author_pattern='<span class="by"><span class="name"><strong>(.*)</strong>&nbsp;.*'
    date_pattern='<div class="FechaContainer" data-fecha="(.*)T.*"></div>'
    ;;

  *logitheque\.com* )
    site='Logitheque'
    author_pattern='<small>Publié le .*</small> <small>Publié par <a target="_blank" href=".*" rel="author" >(.*)</a></small>'
    date_pattern='<small>Publié le (.*)</small> <small>Publié par <a target="_blank" href=".*" rel="author" >.*</a></small>'
    ;;

  *wedemain\.fr* )
    site='We Demain'
    author_pattern='<div class="access">Rédigé par (.*) le .* à .* fois</div>'
    date_pattern='<div class="access">Rédigé par .* le (.*) à .* fois</div>'
    ;;

  *letelegramme\.fr* )
    author='la rédaction'
    date_pattern='<time class="article_date" itemprop="dateModified" datetime=".*">(.*)</time>'
    ;;

  *courrierparlementaire\.com* )
    site='Le Courrier Parlementaire'
    title_pattern='.*<tr><td class=.articleTopHeader. selectedidhead=.11898. style=.background-color:white;.>(.*)</td></tr>'
    author='la rédaction'
    date_pattern="<tr><td style='background-color:white;'><span class='podcastInfo timestamnp' style='font-style:italic;background-color:white;'>Affiché le (.*)</span></td></tr>"
    ;;

  *alternatives-economiques\.fr* )
    site='Alternatives-economiques'
    author='la rédaction'
    date_pattern='le (.* 201.) '
    ;;

  *www\.lopinion\.fr* )
    title_pattern='<title>(.*) . L&#039;Opinion</title>'
    author_pattern='Par <a href=".*" rel="author">(.*)</a>, <span class="poste">.*</span>'
    date_pattern='<div class="article-date">Publié le <time itemprop="datePublished" datetime=".*">(.*) à .*</time> - Mis à jour le <time itemprop="dateModified" datetime=".*">.*</time></div>'
    ;;

  *policyreview\.info* )
    title_pattern='<h1>(.*)</h1>'
    author_pattern='.* by <a href=".*">(.*)</a> on <a href=".*">.*</a>     </div>'
    date_pattern='(.*) by <a href=".*">.*</a> on <a href=".*">.*</a>     </div>'
    ;;

  *scidev\.net* )
    site='SciDev.Net'
    title_pattern='.*<h1>(.*)</h1>.*'
    author_pattern='.*<h4>By (\w* \w*)</h4>.*'
    date_pattern='.*<div id="article-date"><h3>(.*)</h3></div>.*'
    ;;

  *linuxpromagazine\.com* )
    date_pattern='.*<div id="article-date"><h3>(.*)</h3></div>.*'
    ;;

  *industrie-techno\.com* )
    site='Industrie et Technologies'
    date_pattern='<p class="dateAuteurArticle">Par <span itemprop="author">.*</span> publié le <time itemprop="datePublished" datetime=".*">(.*)</time> à .*'
    author_pattern='<p class="dateAuteurArticle">Par <span itemprop="author">(.*)</span> publié le <time itemprop="datePublished" datetime=".*">.*</time> à .*'
    ;;

  *citizenkane\.fr* )
    site='CitizenKane'
    date_pattern='<span class="day">(.*)</span>'
    ;;

  *phonandroid\.com* )
    site='PhonAndroid'
    title_pattern='property="og:title" content="(.*)" /><meta'
    date_pattern='datetime=".*" class="date updated" itemprop="datePublished">(.*)</time>'
    author_pattern='href=".*"  rel="author">(.*)</a></span></span> le <time'
    abstract_pattern='property="og:description" content="(.*)" /><meta'
    ;;

  *comptanoo\.com* )
    site='comptanoo.com'
    date_pattern='<span class="contentstore_content_publishdate">Publié le (.*) </span>.*'
    author='la rédaction'
    abstract_pattern='<meta content="(.*)" name="description" />.*'
    ;;

  *kulturegeek\.fr* )
    site='KultureGeek'
    title_pattern='<meta property="og:title" content="(.*)"/>'
    date_pattern='<span class="date"><span class="icon-clock"></span> <span class="police_info dtreviewed">(.*) @ .*<span class="value-title" title=".*" style="margin-left: -35px;"></span></span>'
    author_pattern='<span class="author"><span class="icon-user"></span> <span class="police_info reviewer"><a href=".*" title="Articles par .*" rel="author">(.*)</a></span></span>'
    abstract_pattern='<meta content="(.*)" name="description" />.*'
    ;;

  *economiematin\.fr* )
    site='EconomieMatin'
    date_pattern='.*<span class="category">(.*)</span> </div>'
    author_pattern='<div class="meta"> <span class="category"><a href=..*.>(.*)</a> </a></span> <span class="category"><a href=.*'
    abstract_pattern='<meta content="(.*)" name="description" />.*'
    ;;

  *lecourrier\.ch* )
    site='Le Courrier'
    title_pattern='.*<h1 class="page-title">(.*)</h1>'
    date_pattern='<div class=.node-created. style=..>(.*)</div>    <div class=.*'
    author_pattern='.*<a href="/journaliste/.*" class="keywords.*">(.*)\(.*\)</a><a.*'
    ;;

  *cowcotland\.com* )
    recode l1..UTF-8 article.html
    title_pattern='<meta property="og:title" content="(.*) - Articles divers" />'
    date_pattern='<div id="infos_news">Posté le (.*) &agrave; .* par .*'
    author_pattern='.* par <a href=".*" title=".*" rel="author">(.*)</a>&nbsp;-.*'
    ;;

  *latribune\.cyber-diego\.com* )
    site='La Tribune de Diego'
    date_pattern='- (.*) ..:..	</span>'
    author='la rédaction'
    ;;

  *lejournal\.cnrs\.fr* )
    site='CNRS Le Journal'
    ;;

  *reporterre\.net* )
    title_pattern='.*<title.*>\s*(.*) - .*</title>'
    author_pattern='<p class="crayon article-soustitre-.* soustitre">(.*)</p>'
    date_pattern='<p class="info-publi"><abbr class="published" title=".*">(.*)</abbr></p>'
    ;;

  *journaldemontreal\.com* )
    title_pattern='<meta property="og:title" content="(.*)" />'
    date_pattern='(.* 20..) ..:..'
    ;;

  *larevuedudigital\.com* )
    site='La Revue du Digital'
    title_pattern='.*<title>(.*) - .*</title>.*'
    author_pattern='class="url fn n" href=".*" title=".*" rel="author">(.*)</a></span><span'
    date_pattern='class="entry-date" datetime=".*">(.*)</time></a><span'
    ;;

   *maddyness\.com* )
    site='Maddyness'
    title_pattern='.*<title>(.*) - .*</title>.*'
		author_pattern='<div class="meta-box"><span>.*</span> par <a href=".*" title="View all posts by .*">(.*)</a> dans .*'
		date_pattern='<div class="meta-box"><span>(.*)</span> par <a href=".*" title="View all posts by .*">.*</a> dans .*'
    ;;

   *netpublic\.fr* )
    site='NetPublic'
    title_pattern='.*<title>.* &raquo; (.*)</title>.*'
		author_pattern='Post&eacute; par <strong>(.*)</strong> le .* <br />'
		date_pattern='Post&eacute; par <strong>.*</strong> le (.*) <br />'
    ;;

   *paris-normandie\.fr* )
    site='paris-normandie.fr'
		title_pattern='.*<title>(.*) - .*</title>.*'
		author_pattern='.*<p><span style="text-transform:uppercase">(.*)</p> <p></span>.*'
		date_pattern='(.*) á ..H...*'
    ;;

   *frenchweb\.fr* )
    site='FrenchWeb'
		title_pattern='<title>(.*) . FrenchWeb.fr</title>'
		author_pattern='<a href=".*" title="Articles par .*" rel="author">(.*)</a>\s*</span>'
		date_pattern='le (.*)'
    ;;

   *letemps\.ch* )
    site='Le Temps'
    ;;
esac
