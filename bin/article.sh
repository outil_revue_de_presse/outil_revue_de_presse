#!/bin/bash

url=$1

cd `dirname $0`/../work
source tokens.sh

dir=tmp
[ ! -d $dir ] && mkdir $dir && echo Generating $dir
cd $dir
#echo $dir

cp ../../template/rp.html .
rm article.txt
w3m $1 -dump > article.txt
rm article.html
wget -cN $1 -O article.html

source ../../bin/patterns.sh

[[ $site == '' ]] && site=`sed -nr "s|^\s*$site_pattern.*|\1|p" article.html` \
  && [[ $site =~ "&#" || $site =~ "eacute" ]] && site=`echo $site|recode html`

[[ $title == '' ]] && title=`sed -nr "s|^\s*$title_pattern.*|\1|p" article.html` \
  && [[ $title =~ "&#" || $title =~ "eacute" ]] && title=`echo $title|recode html`
echo
echo [$site] $title
echo

[[ $author == '' ]] && author=`sed -nr "s|^\s*$author_pattern.*|\1|p" article.html|head -n 1`
[[ $date == '' ]] && date=`sed -nr "s|^\s*$date_pattern.*|\1|p" article.html \
  | sed -r "s/\b./\L&/g"|head -n 1`

echo "Extrait de l'article du site $site par $author en date du $date:"

[[ $abstract == '' ]] && abstract=`sed -nr "s|^\s*$abstract_pattern.*|\1|p" article.html` \
  && [[ $abstract =~ "&#" || $abstract =~ "eacute" ]] && abstract=`echo $abstract|recode html`

echo $abstract
#article=`cat article.txt`
#echo $article

sed -i "s|\$site|$site|g" rp.html
sed -i "s|\$title|\[$site\] $title|g" rp.html
sed -i "s|\$author|$author|g" rp.html
sed -i "s|\$date|$date|g" rp.html
sed -i "s|\$abstract|$abstract|g" rp.html
sed -i "s|\$url|$url|g" rp.html
#sed "/\$article/c $article" rp.html
sed -i "s|\$article||g" rp.html

sed -i "s|\$drupal_token|$article_token|g" rp.html

# Try to prefill some tags based on the article text
IFS=$'\n'
echo
echo -n "Tags found: "
for i in $(grep "taxonomy\[10\]" rp.html|grep -o "/>.*$"|grep -o "\w.*")
do
  grep -qoi "$i" article.html
  [[ $? == 0 ]] && echo -n "$i " && sed -i "s| />$i| checked=\"true\"/>$i|" rp.html
done
echo
echo
unset IFS

w3m rp.html
